﻿namespace Novelab.Triggers
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public class ColliderUnityEvent : UnityEngine.Events.UnityEvent<Collider>
    {

    }

    [RequireComponent(typeof(Collider))]
    public class TriggerDetector : MonoBehaviour
    {
        private Collider myCollider;

        public delegate bool ColliderIntegrityDelegate(Collider _other);
        private ColliderIntegrityDelegate ColliderIntegrityFunction;
        public delegate void ColliderDelegate(Collider _other);
        public ColliderUnityEvent OnEnterDelegate;
        public ColliderUnityEvent OnExitDelegate;

        public enum EnterTrigMode { Everytime, OncePerUniqueElement, OnceUntilEmpty };
        public enum ExitTrigMode { Everytime, OnlyOnLast };
        [SerializeField]
        private EnterTrigMode enterTriggerMode = EnterTrigMode.Everytime;
        [SerializeField]
        private ExitTrigMode exitTriggerMode = ExitTrigMode.Everytime;

        [SerializeField]
        private float destroyedGOCheckDelay = 1;
        private Coroutine destroyedGOCheckCoroutine;
        private WaitForSeconds destroyedGOCheckWait;

        [SerializeField]
        private bool checkCollidingAtStart = false;

        private List<Collider> enteredColliders = new List<Collider>();

        private void Awake()
        {
            myCollider = GetComponent<Collider>();

            destroyedGOCheckWait = new WaitForSeconds(destroyedGOCheckDelay);
        }

        public void Activate()
        {
            if (myCollider != null)
                myCollider.enabled = true;
            if (!checkCollidingAtStart)
                enteredColliders.Clear();
        }

        public void Deactivate()
        {
            if (myCollider != null)
                myCollider.enabled = false;
            enteredColliders.Clear();
            StopDestroyedGOCheckCoroutine();
        }

        private void OnTriggerEnter(Collider _other)
        {
            OnEnterAction(_other);
        }

        public void OnEnterAction(Collider _other)
        {
            if (!CheckColliderIntegrity(_other))
                return;

            if (enterTriggerMode == EnterTrigMode.Everytime
                || (enterTriggerMode == EnterTrigMode.OnceUntilEmpty && enteredColliders.Count == 0))
            {
                TrigOnEnter(_other);
            }

            if (!enteredColliders.Contains(_other))
            {

                enteredColliders.Add(_other);

                if (enterTriggerMode == EnterTrigMode.OncePerUniqueElement)
                    TrigOnEnter(_other);
            }

            if (enteredColliders.Count > 0 && destroyedGOCheckCoroutine == null)
                destroyedGOCheckCoroutine = StartCoroutine(DestroyedGOCheck());
        }

        private void OnTriggerExit(Collider _other)
        {
            OnExitAction(_other);
        }

        public void OnExitAction(Collider _other)
        {
            if (!CheckColliderIntegrity(_other))
                return;

            if (!enteredColliders.Contains(_other))
                return;

            enteredColliders.Remove(_other);

            if (exitTriggerMode == ExitTrigMode.Everytime)
                TrigOnExit(_other);
            else if (exitTriggerMode == ExitTrigMode.OnlyOnLast && enteredColliders.Count == 0)
                TrigOnExit(_other);

            if (enteredColliders.Count == 0)
                StopDestroyedGOCheckCoroutine();
        }

        private bool CheckColliderIntegrity(Collider _other)
        {
            if (ColliderIntegrityFunction == null)
                return true;

            if (ColliderIntegrityFunction(_other))
                return true;

            return false;
        }

        private void TrigOnEnter(Collider _other)
        {
            OnEnterDelegate.Invoke(_other);
        }

        private void TrigOnExit(Collider _other)
        {
            OnExitDelegate.Invoke(_other);
        }

        public void RegisterOnEnter(UnityEngine.Events.UnityAction<Collider> _action)
        {
            OnEnterDelegate.AddListener(_action);
        }

        public void UnregisterOnEnter(UnityEngine.Events.UnityAction<Collider> _action)
        {
            OnEnterDelegate.RemoveListener(_action);
        }

        public void RegisterOnExit(UnityEngine.Events.UnityAction<Collider> _action)
        {
            OnExitDelegate.AddListener(_action);
        }

        public void UnregisterOnExit(UnityEngine.Events.UnityAction<Collider> _action)
        {
            OnExitDelegate.RemoveListener(_action);
        }

        public void SetColliderIntegrityFunction(ColliderIntegrityDelegate _delegate)
        {
            ColliderIntegrityFunction = _delegate;
        }

        private IEnumerator DestroyedGOCheck()
        {
            while (true)
            {
                foreach (Collider item in enteredColliders.ToArray())
                {
                    if (item == null || !item.enabled || !item.gameObject.activeInHierarchy)
                    {
                        OnExitAction(item);
                    }
                }

                yield return destroyedGOCheckWait;
            }
        }

        private void StopDestroyedGOCheckCoroutine()
        {
            if (destroyedGOCheckCoroutine != null)
                StopCoroutine(destroyedGOCheckCoroutine);
            destroyedGOCheckCoroutine = null;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;

            Collider collider = GetComponent<Collider>();
            if (collider.GetType() == typeof(BoxCollider))
            {
                BoxCollider box = GetComponent<BoxCollider>();
                Gizmos.DrawWireCube(transform.position + box.center, new Vector3(transform.lossyScale.x * box.size.x, transform.lossyScale.y * box.size.y, transform.lossyScale.z * box.size.z));
            }
            else if (collider.GetType() == typeof(SphereCollider))
            {
                SphereCollider sphere = GetComponent<SphereCollider>();
                Gizmos.DrawWireSphere(transform.position + sphere.center, transform.lossyScale.x * sphere.radius);
            }
        }

    }
}