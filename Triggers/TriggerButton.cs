﻿namespace Novelab.Triggers
{
    using UnityEngine;
    using UnityEngine.Events;
    using Novelab.Fade;

    public class TriggerButton : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField]
        private TriggerDetector m_trigger;
        [SerializeField]
        private FadeInOut m_fade;

        private enum TriggerButtonActivateType { OnEnter, OnExit };
        [Header("Parameters")]
        [SerializeField]
        private TriggerButtonActivateType activateType;
        [SerializeField]
        private bool activateOnStart = true;
        [SerializeField]
        private bool singleUse = true;
        [SerializeField]
        private UnityEvent m_Events;

        private void Start()
        {
            if(m_fade != null)
                m_fade.SetAlpha(0);

            if(activateOnStart)
                Activate();
        }

        public void Activate()
        {
            m_trigger.RegisterOnEnter(PlayerEnter);
            m_trigger.RegisterOnExit(PlayerExit);
            m_trigger.Activate();

            if (m_fade != null)
                m_fade.FadeIn();
        }

        public void Deactivate()
        {
            m_trigger.UnregisterOnEnter(PlayerEnter);
            m_trigger.UnregisterOnExit(PlayerExit);
            m_trigger.Deactivate();

            if (m_fade != null)
                m_fade.FadeOut();
        }

        private void PlayerEnter(Collider _other)
        {
            if (activateType == TriggerButtonActivateType.OnEnter)
                Use();
        }

        private void PlayerExit(Collider _other)
        {
            if (activateType == TriggerButtonActivateType.OnExit)
                Use();
        }

        private void Use()
        {
            m_Events.Invoke();

            if (singleUse)
                Deactivate();
        }

    }
}