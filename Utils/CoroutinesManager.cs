﻿namespace Novelab.Utils
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    
    public class CoroutinesManager : Singleton<CoroutinesManager>
    {
        private int garbageIterationsDelay = 100;
        private int currentGarbageIterations = 0;

        #region "Base"

        private List<Coroutine> allCoroutines = new List<Coroutine>();

        public Coroutine StartOwnCoroutine(Coroutine _coroutine)
        {
            allCoroutines.Add(_coroutine);

            currentGarbageIterations++;
            if (currentGarbageIterations >= garbageIterationsDelay)
                RefreshOwnCoroutinesList();

            return _coroutine;
        }

        public void StopOwnCoroutine(Coroutine _coroutine)
        {
            if (!allCoroutines.Contains(_coroutine))
                return;

            StopCoroutine(_coroutine);
            allCoroutines.Remove(_coroutine);
        }

        public void RefreshOwnCoroutinesList()
        {
            foreach (Coroutine co in allCoroutines.ToArray())
            {
                if (co == null)
                    allCoroutines.Remove(null);
            }
            currentGarbageIterations = 0;
        }

        public void StopAllOwnCoroutines()
        {
            foreach (Coroutine coroutine in allCoroutines)
            {
                StopCoroutine(coroutine);
            }
            allCoroutines.Clear();
        }

        #endregion

        public delegate void VoidDelegate();
        public delegate void FloatDelegate(float _value);

        #region "Wait"

        public Coroutine StartWaitCoroutine(VoidDelegate _func, float _waitDuration)
        {
            return StartOwnCoroutine(StartCoroutine(WaitCoroutine(_func, _waitDuration)));
        }

        private IEnumerator WaitCoroutine(VoidDelegate _func, float _waitDuration)
        {
            yield return new WaitForSeconds(_waitDuration);

            if (_func != null)
                _func();
        }

        #endregion

        #region "Lerp"

        public Coroutine StartLerpCoroutine(FloatDelegate _func, float _from, float _to, float _duration)
        {
            return StartOwnCoroutine(StartCoroutine(LerpCoroutine(_func, _from, _to, _duration)));
        }

        private IEnumerator LerpCoroutine(FloatDelegate _func, float _from, float _to, float _duration)
        {
            float elapsedTime = 0;

            while (elapsedTime <= _duration)
            {
                if (_func != null)
                    _func(Mathf.Lerp(_from, _to, elapsedTime / _duration));

                yield return null;

                elapsedTime += Time.deltaTime;
            }

            if (_func != null)
                _func(_to);
        }

        #endregion

        #region"Smooth"

        public Coroutine StartSmoothDampCoroutine(FloatDelegate _func, float _from, float _to, float _smoothTime, float _maxSpeed)
        {
            return StartOwnCoroutine(StartCoroutine(SmoothDampCoroutine(_func, _from, _to, _smoothTime, _maxSpeed)));
        }

        private IEnumerator SmoothDampCoroutine(FloatDelegate _func, float _from, float _to, float _smoothTime, float _maxSpeed)
        {
            float elapsedTime = 0;
            float velocity = 0;
            float newValue = _from;
            
            while (Mathf.Abs(_to - newValue) > 0.001f)
            {
                newValue = Mathf.SmoothDamp(newValue, _to, ref velocity, _smoothTime, _maxSpeed, Time.deltaTime);

                if (_func != null)
                    _func(newValue);

                yield return null;

                elapsedTime += Time.deltaTime;
            }

            if (_func != null)
                _func(_to);
        }

        #endregion

        #region "Curve"

        public Coroutine StartCurveCoroutine(FloatDelegate _func, float _min, float _max, float _duration, AnimationCurve _curve)
        {
            return StartOwnCoroutine(StartCoroutine(CurveCoroutine(_func, _min, _max, _duration, _curve)));
        }

        private IEnumerator CurveCoroutine(FloatDelegate _func, float _min, float _max, float _duration, AnimationCurve _curve)
        {
            float elapsedTime = 0;

            while (elapsedTime <= _duration)
            {
                if (_func != null)
                    _func(Mathf.Lerp(_min, _max, _curve.Evaluate(elapsedTime / _duration)));

                yield return null;

                elapsedTime += Time.deltaTime;
            }

            if (_func != null)
                _func(Mathf.Lerp(_min, _max, _curve.Evaluate(1f)));
        }

        #endregion

    }
}