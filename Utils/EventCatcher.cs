﻿namespace Novelab.Utils
{
    /// <summary>
    /// The event catcher is a way to catch standard unity event from standard C# event
    /// </summary>
    public class EventCatcher : Singleton<EventCatcher>
    {
        public event System.Action AwakeEvent;
        public event System.Action UpdateEvent;
        public event System.Action LateUpdateEvent;
        public event System.Action FixedUpdateEvent;
        public event System.Action ApplicationQuitEvent;
        public event System.Action<bool> ApplicationPauseEvent;
        public event System.Action<bool> ApplicationFocusEvent;
        public event System.Action DestroyEvent;

        protected override void Awake()
        {
            base.Awake();
            if (AwakeEvent != null)
                AwakeEvent();
        }

        private void Update()
        {
            if (UpdateEvent != null)
                UpdateEvent();
        }

        private void LateUpdate()
        {
            if (LateUpdateEvent != null)
                LateUpdateEvent();
        }

        private void FixedUpdate()
        {
            if (FixedUpdateEvent != null)
                FixedUpdateEvent();
        }

        private void OnApplicationQuit()
        {
            if (ApplicationQuitEvent != null)
                ApplicationQuitEvent();
        }

        private void OnApplicationFocus(bool focus)
        {
            if (ApplicationFocusEvent != null)
                ApplicationFocusEvent(focus);
        }

        private void OnApplicationPause(bool pause)
        {
            if (ApplicationPauseEvent != null)
                ApplicationPauseEvent(pause);
        }

        private void OnDestroy()
        {
            if (DestroyEvent != null)
                DestroyEvent();
        }
    }
}
