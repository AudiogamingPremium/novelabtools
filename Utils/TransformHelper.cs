﻿using UnityEngine;


namespace Novelab.Utils
{
    public static class TransformHelper
    {

        public static void FindChildRecursively(Transform _parent, ref Transform _foundChild, string _childName)
        {
            _foundChild = _parent.Find(_childName);

            for (int i = 0; i < _parent.childCount; i++)
            {
                if (_foundChild != null)
                    return;
                FindChildRecursively(_parent.GetChild(i), ref _foundChild, _childName);
            }
        }

        /// <summary>
        /// Clamp an angle between min & max (-180/180)
        /// </summary>
        /// <param name="_angle">Angle in degrees</param>
        /// <param name="_min">Min value between -180 & 180</param>
        /// <param name="_max">Max value between -180 & 180</param>
        /// <returns></returns>
        public static float ClampAngle180(float _angle, float _min, float _max)
        {
            return Mathf.Clamp(AngleTo180(_angle), _min, _max);
        }

        public static float AngleTo180(float _angle)
        {
            if (_angle > 180f)
                return AngleTo180(_angle - 360f);
            else if (_angle < -180f)
                return AngleTo180(_angle + 360f);
            else
                return _angle;
        }

        /// <summary>
        /// Clamp an angle between min & max (0/360)
        /// </summary>
        /// <param name="_angle">Angle in degrees</param>
        /// <param name="_min">Min value between 0 & 360</param>
        /// <param name="_max">Max value between 0 & 360</param>
        /// <returns></returns>
        public static float ClampAngle360(float _angle, float _min, float _max)
        {
            return Mathf.Clamp(AngleTo360(_angle), _min, _max);
        }

        public static float AngleTo360(float _angle)
        {
            if (_angle > 360f)
                return AngleTo360(_angle - 360f);
            else if (_angle < 0)
                return AngleTo360(_angle + 360f);
            else
                return _angle;
        }

    }
}
