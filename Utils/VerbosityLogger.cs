﻿using System.Diagnostics;
using UnityEngine;

namespace Novelab.Utils
{
    public abstract class VerbosityLogger : MonoBehaviour
    {
        private enum Verbosity { None = 0, Errors = 1, Warnings = 2, Low = 3, Medium = 4, High = 5 };
        public enum LogPriority { High = 3, Medium = 4, Low = 5 };
        private enum Type { Log, Warning, Error };

        [Header("Logs")]
        [SerializeField]
        private Verbosity m_verbosity = Verbosity.High;


        protected void Log(string _log, LogPriority _logPriority = LogPriority.Low)
        {
            LogDefault(Type.Log, _log, _logPriority);
        }

        protected void LogWarning(string _log)
        {
            LogDefault(Type.Warning, _log);
        }

        protected void LogError(string _log)
        {
            LogDefault(Type.Error, _log);
        }

        StackTrace stackTrace;
        private void LogDefault(Type _type, string _log, LogPriority _logPriority = LogPriority.Low)
        {

            switch (_type)
            {
                case Type.Log:
                    if ((int)_logPriority <=(int) m_verbosity)
                        UnityEngine.Debug.Log(GetStartLog() + _log);
                    break;
                case Type.Warning:
                    if (m_verbosity >= Verbosity.Warnings)
                        UnityEngine.Debug.LogWarning(GetStartLog() + _log);
                    break;
                case Type.Error:
                    if (m_verbosity >= Verbosity.Errors)
                        UnityEngine.Debug.LogError(GetStartLog() + _log);
                    break;
            }
        }

        private string GetStartLog()
        {
            stackTrace = new StackTrace();
            return "[" + gameObject.name + " : " + GetType().ToString() + "] " + stackTrace.GetFrame(3).GetMethod().Name + " => ";
        }
    }
}