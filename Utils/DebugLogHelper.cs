﻿namespace Novelab.Utils
{
    using UnityEngine;

    public static class DebugLogHelper
    {

        public static string Vector3ToString(Vector3 _vector)
        {
            return "x:" + _vector.x.ToString("0.0000") + " y:" + _vector.y.ToString("0.0000") + " z:" + _vector.z.ToString("0.0000");
        }

        public static string QuaternionToString(Quaternion _quat)
        {
            return "x:" + _quat.x.ToString("0.0000") + " y:" + _quat.y.ToString("0.0000") + " z:" + _quat.z.ToString("0.0000") + " w:" + _quat.w.ToString("0.0000");
        }

    }

}