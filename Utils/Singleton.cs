﻿using UnityEngine;

namespace Novelab.Utils
{
    public abstract class Singleton<T> : MonoBehaviour where T : Component
    {

        #region Fields

        /// <summary>
        /// The instance.
        /// </summary>
        private static T instance;

        private static bool _shuttingDown;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static T Instance
        {
            get
            {
                Debug.Assert(!_shuttingDown, string.Format(
                    "Warning: you are trying to create {0} on application quit", typeof(T)));
                if (instance == null)
                {
                    instance = FindObjectOfType<T>();
                    if (instance == null)
                    {
                        GameObject obj = new GameObject(typeof(T).Name);
                        instance = obj.AddComponent<T>();
                    }
                }
                return instance;
            }
        }

        public static bool IsAvailable 
        {
            get
            {
                return !_shuttingDown && instance != null;
            }
        }

        protected virtual bool IsPersistent 
        {
            get {
                return true;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Use this for initialization.
        /// </summary>
        protected virtual void Awake()
        {
            if (instance == null)
            {
                instance = this as T;
                if (IsPersistent)
                {
                    DontDestroyOnLoad(transform.root.gameObject);
                }
            }
            else
            {
                Destroy(gameObject);
                return;
            }
        }

        private void OnApplicationQuit() {
            _shuttingDown = true;
        }

        #endregion

    }
}