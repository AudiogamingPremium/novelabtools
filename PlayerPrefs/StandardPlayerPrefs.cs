﻿using UnityEngine;

namespace Novelab.PlayerPrefs
{
    public class StandardPlayerPrefs : IPlayerPrefs
    {
        public void DeleteAll()
        {
            UnityEngine.PlayerPrefs.DeleteAll();
        }

        public void DeleteKey(string key)
        {
            UnityEngine.PlayerPrefs.DeleteKey(key);
        }

        public float GetFloat(string key, float defaultValue)
        {
            return UnityEngine.PlayerPrefs.GetFloat(key, defaultValue);
        }

        public float GetFloat(string key)
        {
            return UnityEngine.PlayerPrefs.GetFloat(key);
        }

        public int GetInt(string key, int defaultValue)
        {
            return UnityEngine.PlayerPrefs.GetInt(key, defaultValue);
        }

        public int GetInt(string key)
        {
            return UnityEngine.PlayerPrefs.GetInt(key);
        }

        public string GetString(string key, string defaultValue)
        {
            return UnityEngine.PlayerPrefs.GetString(key, defaultValue);
        }

        public string GetString(string key)
        {
            return UnityEngine.PlayerPrefs.GetString(key);
        }

        public bool HasKey(string key)
        {
            return UnityEngine.PlayerPrefs.HasKey(key);
        }

        public void Save()
        {
            UnityEngine.PlayerPrefs.Save();
        }

        public void SetFloat(string key, float value)
        {
            UnityEngine.PlayerPrefs.SetFloat(key, value);
        }

        public void SetInt(string key, int value)
        {
            UnityEngine.PlayerPrefs.SetInt(key, value);
        }

        public void SetString(string key, string value)
        {
            UnityEngine.PlayerPrefs.SetString(key, value);
        }
    }
}