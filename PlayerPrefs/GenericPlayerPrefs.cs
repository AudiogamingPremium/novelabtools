﻿#if UNITY_SWITCH
using Novelab.NintendoSwitch;
#endif
using Novelab.Utils;

namespace Novelab.PlayerPrefs
{
    /// <summary>
    /// A generic player prefs extensible for all platforms, use this to be able to extend PlayerPrefs to other platforms
    /// </summary>
    public class GenericPlayerPrefs : Singleton<GenericPlayerPrefs>, IPlayerPrefs
    {
        private IPlayerPrefs playerPrefsProvider = new StandardPlayerPrefs();

        protected override void Awake()
        {
            base.Awake();
#if UNITY_SWITCH && !UNITY_EDITOR
            playerPrefsProvider = NintendoSwitchPlayerPrefs.Instance;
#endif
        }

        #region Functions
        public void DeleteAll()
        {
            playerPrefsProvider.DeleteAll();
        }

        public void DeleteKey(string key)
        {
            playerPrefsProvider.DeleteKey(key);
        }

        public float GetFloat(string key, float defaultValue)
        {
            return playerPrefsProvider.GetFloat(key, defaultValue);
        }

        public float GetFloat(string key)
        {
            return playerPrefsProvider.GetFloat(key);
        }

        public int GetInt(string key, int defaultValue)
        {
            return playerPrefsProvider.GetInt(key, defaultValue);
        }

        public int GetInt(string key)
        {
            return playerPrefsProvider.GetInt(key);
        }

        public string GetString(string key, string defaultValue)
        {
            return playerPrefsProvider.GetString(key, defaultValue);
        }

        public string GetString(string key)
        {
            return playerPrefsProvider.GetString(key);
        }

        public bool HasKey(string key)
        {
            return playerPrefsProvider.HasKey(key);
        }

        public void Save()
        {
            playerPrefsProvider.Save();
        }

        public void SetFloat(string key, float value)
        {
            playerPrefsProvider.SetFloat(key, value);
        }

        public void SetInt(string key, int value)
        {
            playerPrefsProvider.SetInt(key, value);
        }

        public void SetString(string key, string value)
        {
            playerPrefsProvider.SetString(key, value);
        }
        #endregion

    }
}