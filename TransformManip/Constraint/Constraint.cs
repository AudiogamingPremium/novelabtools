﻿namespace Novelab.TransformManip
{
    using System;
    using UnityEngine;

    [ExecuteInEditMode]
    public class Constraint : MonoBehaviour
    {
        public enum ConstraintType { Parent, Point, Orient, Scale, Aim };
        [Flags]
        public enum AxisFlag
        {
            X = 1 << 0,
            Y = 1 << 1,
            Z = 1 << 2
        }

        [SerializeField]
        private bool playInEditor = false;
        public bool PlayInEditor
        {
            get { return playInEditor; }
            set { playInEditor = value; }
        }
        [SerializeField]
        private bool initialized = false;

        //Transforms
        [SerializeField]
        private Transform targetTrans;
        [SerializeField]
        private Transform myTrans;

        // Constraint
        [SerializeField]
        private ConstraintType selectedConstraint = ConstraintType.Parent;

        // Parent
        [SerializeField]
        private bool snapPosition;
        [SerializeField]
        private bool lockScale;

        // Translation
        [SerializeField]
        private AxisFlag translationFlag;
        [SerializeField]
        private bool translationKeepOffset;
        [SerializeField]
        private Vector3 translationOffset;

        // Rotation
        [SerializeField]
        private AxisFlag rotationFlag;
        [SerializeField]
        private bool rotationKeepOffset;
        [SerializeField]
        private Vector3 rotationOffset;

        // Scale
        [SerializeField]
        private AxisFlag scaleFlag;
        [SerializeField]
        private bool scaleKeepOffset;
        [SerializeField]
        private Vector3 scaleOffset;

        // Aim
        [SerializeField]
        private bool aimKeepOffset;
        [SerializeField]
        private Vector3 aimRotationOffset;
        [SerializeField]
        private AxisFlag aimLocalUpVector = AxisFlag.Y;
        [SerializeField]
        private Vector3 aimUpVector;

        //private Vector3 initTargetPos;
        private Vector3 initTargetRot;
        private Vector3 initTargetScale;

        private Vector3 deltaRot;
        private Vector3 deltaScale;


        private void Awake()
        {
            if (!(Application.isEditor && !Application.isPlaying) && targetTrans == null)
            {
                Debug.LogError("No target for constraint on " + name);
                Destroy(this);
            }
        }

        private void Init()
        {
            myTrans = GetComponent<Transform>();

            if (targetTrans == null)
            {
                Debug.LogError("[Constraint]No target transform for " + name);
                return;
            }

            //initTargetPos = targetTrans.position;
            initTargetRot = targetTrans.eulerAngles;
            initTargetScale = targetTrans.localScale;

            if (selectedConstraint == ConstraintType.Parent || translationKeepOffset)
                translationOffset = myTrans.position - targetTrans.position;
            if (selectedConstraint == ConstraintType.Parent || rotationKeepOffset)
                rotationOffset = myTrans.eulerAngles - targetTrans.eulerAngles;
            if (selectedConstraint == ConstraintType.Parent || scaleKeepOffset)
                scaleOffset = myTrans.localScale - targetTrans.localScale;

            if (selectedConstraint == ConstraintType.Aim)
            {
                switch (aimLocalUpVector)
                {
                    case AxisFlag.X:
                        aimUpVector = myTrans.right;
                        break;
                    case AxisFlag.Y:
                        aimUpVector = myTrans.up;
                        break;
                    case AxisFlag.Z:
                        aimUpVector = myTrans.forward;
                        break;
                }

                if (aimKeepOffset)
                {
                    Vector3 toVector = (targetTrans.position - myTrans.position).normalized;
                    Quaternion tempQuaternion = Quaternion.LookRotation(toVector, aimUpVector);
                    tempQuaternion = Quaternion.Inverse(tempQuaternion) * (myTrans.rotation);
                    aimRotationOffset = tempQuaternion.eulerAngles;
                }
            }


            initialized = true;
        }

        private void Update()
        {
            if (Application.isEditor && !Application.isPlaying && !playInEditor)
                return;

            if (!initialized)
                Init();

            switch (selectedConstraint)
            {
                case ConstraintType.Parent:
                    UpdateParent();
                    break;
                case ConstraintType.Point:
                    UpdatePoint();
                    break;
                case ConstraintType.Orient:
                    UpdateOrient();
                    break;
                case ConstraintType.Scale:
                    UpdateScale();
                    break;
                case ConstraintType.Aim:
                    UpdateAim();
                    break;
                default:
                    break;
            }
        }

        private void UpdateParent()
        {
            if (targetTrans == null)
                return;

            // On récupère les delta de la cible
            deltaRot = targetTrans.localEulerAngles - initTargetRot;
            deltaScale = targetTrans.localScale - initTargetScale;

            // On fait pivoter l'objet lié de la même manière que sa target
            myTrans.localEulerAngles = targetTrans.localEulerAngles + rotationOffset;

            // On update la scale de la même manière que l'objet lié
            if (!lockScale)
                myTrans.localScale = targetTrans.localScale + scaleOffset;

            // On positionne l'objet en scalant son offset de position et en tenant en compte la rotation
            myTrans.position = targetTrans.position + (!snapPosition
                ? (Quaternion.Euler(deltaRot) * (translationOffset + new Vector3(translationOffset.x * deltaScale.x, translationOffset.y * deltaScale.y, translationOffset.z * deltaScale.z)))
                : Vector3.zero);
        }

        private void UpdatePoint()
        {
            if (targetTrans == null)
                return;

            Vector3 newPos = myTrans.position;

            if ((translationFlag & AxisFlag.X) == AxisFlag.X)
                newPos.x = targetTrans.position.x + translationOffset.x;
            if ((translationFlag & AxisFlag.Y) == AxisFlag.Y)
                newPos.y = targetTrans.position.y + translationOffset.y;
            if ((translationFlag & AxisFlag.Z) == AxisFlag.Z)
                newPos.z = targetTrans.position.z + translationOffset.z;

            myTrans.position = newPos;
        }

        private void UpdateOrient()
        {
            if (targetTrans == null)
                return;

            Vector3 newRotation = myTrans.eulerAngles;

            if ((rotationFlag & AxisFlag.X) == AxisFlag.X)
                newRotation.x = targetTrans.eulerAngles.x + rotationOffset.x;
            if ((rotationFlag & AxisFlag.Y) == AxisFlag.Y)
                newRotation.y = targetTrans.eulerAngles.y + rotationOffset.y;
            if ((rotationFlag & AxisFlag.Z) == AxisFlag.Z)
                newRotation.z = targetTrans.eulerAngles.z + rotationOffset.z;

            myTrans.eulerAngles = newRotation;
        }

        private void UpdateAim()
        {
            if (targetTrans == null)
                return;

            Vector3 toVector = targetTrans.position - myTrans.position;
            myTrans.rotation = Quaternion.LookRotation(toVector, aimUpVector) * Quaternion.Euler(aimRotationOffset);
        }

        private void UpdateScale()
        {
            if (targetTrans == null)
                return;

            Vector3 newScale = myTrans.localScale;

            if ((scaleFlag & AxisFlag.X) == AxisFlag.X)
                newScale.x = targetTrans.localScale.x + scaleOffset.x;
            if ((scaleFlag & AxisFlag.Y) == AxisFlag.Y)
                newScale.y = targetTrans.localScale.y + scaleOffset.y;
            if ((scaleFlag & AxisFlag.Z) == AxisFlag.Z)
                newScale.z = targetTrans.localScale.z + scaleOffset.z;

            myTrans.localScale = newScale;
        }
    }
}