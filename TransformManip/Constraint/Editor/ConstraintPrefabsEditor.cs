﻿namespace Novelab.TransformManip
{
    using UnityEngine;
    using UnityEditor;

    [InitializeOnLoad]
    internal class ConstraintPrefabsEditor
    {
        static ConstraintPrefabsEditor()
        {
            UnityEditor.PrefabUtility.prefabInstanceUpdated += OnPrefabInstanceUpdate;
        }

        static void OnPrefabInstanceUpdate(GameObject instance)
        {
#if UNITY_2018_2_OR_NEWER
            GameObject prefab = UnityEditor.PrefabUtility.GetCorrespondingObjectFromSource(instance) as GameObject;
#else
            GameObject prefab = UnityEditor.PrefabUtility.GetPrefabParent(instance) as GameObject;
#endif
            Constraint[] allConstraints = prefab.GetComponentsInChildren<Constraint>();

            foreach (Constraint constraint in allConstraints)
            {
                constraint.PlayInEditor = false;
            }
        }
    }
}