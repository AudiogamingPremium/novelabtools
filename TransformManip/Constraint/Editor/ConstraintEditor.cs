﻿namespace Novelab.TransformManip
{
    using UnityEngine;
    using UnityEditor;

    [CustomEditor(typeof(Constraint))]
    [CanEditMultipleObjects]
    public class ConstraintEditor : Editor
    {
        private SerializedProperty targetTrans;
        private SerializedProperty selectedConstraint;
        private SerializedProperty playInEditor;
        private SerializedProperty initialized;

        private SerializedProperty snapPosition;
        private SerializedProperty lockScale;

        private SerializedProperty translationFlag;
        private SerializedProperty translationKeepOffset;
        private SerializedProperty translationOffset;

        private SerializedProperty rotationFlag;
        private SerializedProperty rotationKeepOffset;
        private SerializedProperty rotationOffset;

        private SerializedProperty scaleFlag;
        private SerializedProperty scaleKeepOffset;
        private SerializedProperty scaleOffset;

        private SerializedProperty aimKeepOffset;
        private SerializedProperty aimRotationOffset;
        private SerializedProperty aimLocalUpVector;

        void OnEnable()
        {
            targetTrans = serializedObject.FindProperty("targetTrans");
            selectedConstraint = serializedObject.FindProperty("selectedConstraint");
            playInEditor = serializedObject.FindProperty("playInEditor");
            initialized = serializedObject.FindProperty("initialized");

            snapPosition = serializedObject.FindProperty("snapPosition");
            lockScale = serializedObject.FindProperty("lockScale");

            translationFlag = serializedObject.FindProperty("translationFlag");
            translationKeepOffset = serializedObject.FindProperty("translationKeepOffset");
            translationOffset = serializedObject.FindProperty("translationOffset");

            rotationFlag = serializedObject.FindProperty("rotationFlag");
            rotationKeepOffset = serializedObject.FindProperty("rotationKeepOffset");
            rotationOffset = serializedObject.FindProperty("rotationOffset");

            scaleFlag = serializedObject.FindProperty("scaleFlag");
            scaleKeepOffset = serializedObject.FindProperty("scaleKeepOffset");
            scaleOffset = serializedObject.FindProperty("scaleOffset");

            aimKeepOffset = serializedObject.FindProperty("aimKeepOffset");
            aimRotationOffset = serializedObject.FindProperty("aimRotationOffset");
            aimLocalUpVector = serializedObject.FindProperty("aimLocalUpVector");
        }

        public override void OnInspectorGUI()
        {
            // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
            serializedObject.Update();

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Target", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(targetTrans);

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Constraint", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(selectedConstraint);

            switch ((Constraint.ConstraintType)selectedConstraint.intValue)
            {
                case Constraint.ConstraintType.Parent:
                    EditorGUILayout.LabelField("Translation", EditorStyles.boldLabel);
                    EditorGUILayout.PropertyField(snapPosition);
                    EditorGUILayout.LabelField("Scale", EditorStyles.boldLabel);
                    EditorGUILayout.PropertyField(lockScale);
                    break;
                case Constraint.ConstraintType.Point:
                    EditorGUILayout.Space();
                    EditorGUILayout.LabelField("Translation", EditorStyles.boldLabel);
                    DrawTranslationFlagEnumMask("Translation flag", translationFlag);
                    EditorGUILayout.PropertyField(translationKeepOffset);
                    if (!translationKeepOffset.boolValue)
                        EditorGUILayout.PropertyField(translationOffset);
                    break;
                case Constraint.ConstraintType.Orient:
                    EditorGUILayout.Space();
                    EditorGUILayout.LabelField("Rotation", EditorStyles.boldLabel);
                    DrawTranslationFlagEnumMask("Rotation flag", rotationFlag);
                    EditorGUILayout.PropertyField(rotationKeepOffset);
                    if (!rotationKeepOffset.boolValue)
                        EditorGUILayout.PropertyField(rotationOffset);
                    break;
                case Constraint.ConstraintType.Scale:
                    EditorGUILayout.Space();
                    EditorGUILayout.LabelField("Scale", EditorStyles.boldLabel);
                    DrawTranslationFlagEnumMask("Scale flag", scaleFlag);
                    EditorGUILayout.PropertyField(scaleKeepOffset);
                    if (!scaleKeepOffset.boolValue)
                        EditorGUILayout.PropertyField(scaleOffset);
                    break;
                case Constraint.ConstraintType.Aim:
                    EditorGUILayout.Space();
                    EditorGUILayout.LabelField("Aim", EditorStyles.boldLabel);
                    EditorGUILayout.PropertyField(aimLocalUpVector);
                    EditorGUILayout.PropertyField(aimKeepOffset);
                    if (!aimKeepOffset.boolValue)
                        EditorGUILayout.PropertyField(aimRotationOffset);
                    break;
                default:
                    break;
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            if (playInEditor.boolValue)
            {
                GUI.color = Color.red;
                if (GUILayout.Button("Stop in editor"))
                {
                    playInEditor.boolValue = false;
                    initialized.boolValue = false;
                }
            }
            else
            {
                GUI.color = Color.green;
                if (GUILayout.Button("Play in editor"))
                {
                    initialized.boolValue = false;
                    playInEditor.boolValue = true;
                }
            }

            // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
            serializedObject.ApplyModifiedProperties();
        }

        private void DrawTranslationFlagEnumMask(string _name, SerializedProperty _maskPropertie)
        {
            _maskPropertie.intValue = (int)((Constraint.AxisFlag)EditorGUILayout.EnumFlagsField(_name, (Constraint.AxisFlag)_maskPropertie.intValue));

            if (_maskPropertie.intValue < 0)
            {
                int bits = 0;
                foreach (Constraint.AxisFlag enumValue in System.Enum.GetValues(typeof(Constraint.AxisFlag)))
                {
                    int checkBit = _maskPropertie.intValue & (int)enumValue;
                    if (checkBit != 0)
                        bits |= (int)enumValue;
                }

                _maskPropertie.intValue = bits;
            }
        }

    }
}