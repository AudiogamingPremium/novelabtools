﻿namespace Novelab.TransformManip
{
    using UnityEngine;

    public class RotateTrans : MonoBehaviour
    {
        private Transform m_trans;

        [SerializeField]
        private Vector3 rotationSpeed;
        [SerializeField]
        private bool local = false;

        private void Awake()
        {
            m_trans = GetComponent<Transform>();
        }

        private void Update()
        {
            m_trans.Rotate(rotationSpeed * Time.deltaTime, local ? Space.Self : Space.World);
        }
    }
}