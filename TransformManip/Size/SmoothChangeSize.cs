﻿namespace Novelab.TransformManip
{
    using UnityEngine;
    using Novelab.Utils;

    public abstract class SmoothChangeSize : MonoBehaviour
    {
        [Header("Parameters")]
        public float duration = 2;
        public AnimationCurve sizeCurve;
        protected Vector3 startSize;
        protected Vector3 destSize;
        
        public UnityEngine.Events.UnityEvent OnEndChangeSize;

        protected Coroutine changeSizeCo;

        protected abstract void SetSize(Vector3 _color);
        protected abstract Vector3 GetSize();

        public void ChangeSize(Vector3 _destSize, float _duration = -1, AnimationCurve _sizeCurve = null)
        {
            startSize = GetSize();
            destSize = _destSize;

            if (_duration == -1)
                _duration = duration;

            if (_sizeCurve == null)
                _sizeCurve = sizeCurve;

            if (changeSizeCo != null)
                CoroutinesManager.Instance.StopOwnCoroutine(changeSizeCo);

            if (gameObject.activeInHierarchy)
                changeSizeCo = CoroutinesManager.Instance.StartCurveCoroutine(UpdateSize, 0, 1, _duration, _sizeCurve);
        }

        public void UpdateSize(float _lerp)
        {
            SetSize(Vector3.Lerp(startSize, destSize, _lerp));

            if (_lerp == 1)
                TrigEndChangeSize();
        }

        private void TrigEndChangeSize()
        {
            OnEndChangeSize.Invoke();
        }

        public void RegisterEndChangeSize(UnityEngine.Events.UnityAction _action)
        {
            OnEndChangeSize.AddListener(_action);
        }

        public void UnregisterEndChangeSize(UnityEngine.Events.UnityAction _action)
        {
            OnEndChangeSize.RemoveListener(_action);
        }

    }
}