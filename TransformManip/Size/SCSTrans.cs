﻿namespace Novelab.TransformManip
{
    using System.Collections.Generic;
    using UnityEngine;

    public class SCSTrans : SmoothChangeSize
    {
        [Header("Transform")]
        public List<Transform> resizedTransforms = new List<Transform>();

        protected override Vector3 GetSize()
        {
            if (resizedTransforms.Count == 0)
            {
                Debug.Log("[SCSTrans] No trans set to " + name);
                return Vector3.zero;
            }

            return resizedTransforms[0].localScale;
        }

        protected override void SetSize(Vector3 _size)
        {
            if (resizedTransforms.Count == 0)
            {
                Debug.Log("[SCSTrans] No trans set to " + name);
                return;
            }

            for (int i = 0; i < resizedTransforms.Count; i++)
            {
                if (resizedTransforms[i] == null)
                    continue;

                resizedTransforms[i].localScale = _size;
            }
        }
    }
}