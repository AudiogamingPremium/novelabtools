﻿namespace Novelab.TransformManip
{
    using System.Collections;
    using UnityEngine;

    [RequireComponent(typeof(SmoothChangeSize))]
    public class SCSPingPong : MonoBehaviour
    {
        [SerializeField]
        private SmoothChangeSize scsElement;

        public float waitingTime = 0.2f;
        public Vector3 minScale;
        public Vector3 maxScale;

        public AnimationCurve growCurve;
        public AnimationCurve reduceCurve;

        private bool goToMax = false;

        [SerializeField]
        private bool autoStart = false;

        private Coroutine m_coroutine;

        private void Awake()
        {
            scsElement = GetComponent<SmoothChangeSize>();
        }

        private void Start()
        {
            scsElement.RegisterEndChangeSize(OnEndChangeSize);
        }

        private void OnEnable()
        {
            if (autoStart)
                StartPingPong();
        }

        private void OnDisable()
        {
            StopCoroutine();
        }

        public void StartPingPong()
        {
            GoToMax();
        }

        private void OnEndChangeSize()
        {
            StopCoroutine();
            if (gameObject.activeInHierarchy)
                m_coroutine = StartCoroutine(WaitCoroutine());
        }

        private IEnumerator WaitCoroutine()
        {
            yield return new WaitForSeconds(waitingTime);

            if (goToMax)
                GoToMin();
            else
                GoToMax();
        }

        private void StopCoroutine()
        {
            if (m_coroutine != null)
                StopCoroutine(m_coroutine);
        }

        private void GoToMax()
        {
            goToMax = true;
            scsElement.ChangeSize(maxScale, _sizeCurve: growCurve);
        }

        private void GoToMin()
        {
            goToMax = false;
            scsElement.ChangeSize(minScale, _sizeCurve: reduceCurve);
        }

    }
}