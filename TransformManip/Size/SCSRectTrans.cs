﻿namespace Novelab.TransformManip
{
    using System.Collections.Generic;
    using UnityEngine;

    public class SCSRectTrans : SmoothChangeSize
    {
        [Header("RectTransform")]
        public List<RectTransform> resizedRectTransforms = new List<RectTransform>();

        protected override Vector3 GetSize()
        {
            if (resizedRectTransforms.Count == 0)
            {
                Debug.LogError("[SCSRectTrans] no recttrans set to " + name);
                return Vector3.zero;
            }

            return resizedRectTransforms[0].localScale;
        }

        protected override void SetSize(Vector3 _size)
        {
            if (resizedRectTransforms.Count == 0)
            {
                Debug.LogError("[SCSRectTrans] No recttrans set to " + name);
                return;
            }

            for (int i = 0; i < resizedRectTransforms.Count; i++)
            {
                if (resizedRectTransforms[i] == null)
                    continue;

                resizedRectTransforms[i].localScale = _size;
            }
        }
    }
}