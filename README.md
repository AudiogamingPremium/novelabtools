# NovelabTools

D�p�t pour les tools g�n�riques de Novelab.

## Utilisation

Pour int�grer la librairie NovelabTools dans un projet, utiliser le d�p�t comme un git submodule.

D�tail de l'utilisation dans SourceTree ici : [Using Git Submodules in SourceTree](https://confluence.atlassian.com/sourcetreekb/adding-a-submodule-subtree-with-sourcetree-785332086.html).

Le submodule agit ensuite comme un d�p�t git classique, � l'exception que les changements faits dans le d�p�t du submodule n'affectent pas le d�p�t parent.
Pour mettre � jour le submodule, il suffit alors de mettre � jour le submodule comme pour un d�p�t classique.

Une fois le module int�gr� et push�, le module n'a plus � �tre int�gr� par les autres collaborateurs, il sera automatiquement r�cup�r�.

## Contenu

- Fade
Un syst�me de fade in fade �ut utilisant des coroutines. Un script est disponible en fonction du composant vis�.

- PlayerPrefs
Une impl�mentation g�n�rique des PlayerPrefs unity pouvant �tre facilement �tendue � d'autres plateformes (l'impl�mentation Nintendo Switch est faite)

-SmoothChangeColor
Un syst�me de Smooth de couleur utilisant des coroutines. Un script est disponible en fonction du composant vis�.

- Subtitles
Syst�me de gestion de sous-titres bas� sur des s�quences de SRT. Un manager permet de lancer la s�quence voulue. Des composants permettent de r�cup�r� le sous titre en cour et de l'afficher.

- TransformManip
Tools de motification au court du temps des transforms et RectTransform

- Triggers
Syst�me de lancement d'action via trigger. Il est possible de s'abonner et de rajouter une fonction qui va valider ou non si l'action s'effectue. (ColliderIntegrityFunction)

- Utils
Utilitaires tr�s g�n�ralistes pouvant �tre utilis�s dans tout types de projet.


## Note aux devs

Tous les scripts d�velopp�s et push�s sur le d�p�t doivent �tre dans le namespace "Novelab.NomTool".
L'organisation des namespaces doit �tre coh�rente et doit coller au mieux � la hi�rarchie des dossiers.