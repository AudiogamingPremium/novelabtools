﻿namespace Novelab.Subtitles
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;
    using UnityEditorInternal;
    using System.IO;

    [CustomEditor(typeof(SubtitleSequence))]
    public class SubtitleSequenceEditor : Editor
    {
        private ReorderableList reorderableElementsList;

        private void OnEnable()
        {
            reorderableElementsList = new ReorderableList(serializedObject, serializedObject.FindProperty("subtitleElementsList"), true, true, true, true);
            reorderableElementsList.drawHeaderCallback = DrawHeader;
            reorderableElementsList.drawElementCallback = DrawReorderableSequenceElement;
            reorderableElementsList.onAddCallback = AddSequenceToReorderableList;
            reorderableElementsList.elementHeightCallback = ElementHeight;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Subtitle sequences", EditorStyles.boldLabel);
            reorderableElementsList.DoLayoutList();

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            if (GUILayout.Button("Import from SRT"))
            {
                if (EditorUtility.DisplayDialog("Import " + target.name + " from SRT", "Etes vous sûr de vouloir importer cette séquence depuis un fichier SRT ?\nToutes les données actuelles de la séquence seront perdues.", "Confirmer", "Annuler"))
                {
                    ImportFromSRT();
                }
            }
            if (GUILayout.Button("Export to SRT"))
            {
                ExportToSRT("srt");
            }
            if (GUILayout.Button("Export to TextAsset"))
            {
                ExportToSRT("txt");
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawHeader(Rect rect)
        {
            EditorGUI.LabelField(new Rect(rect.x + 10, rect.y, 30, EditorGUIUtility.singleLineHeight), "ID", EditorStyles.centeredGreyMiniLabel);
            EditorGUI.LabelField(new Rect(rect.x + 45, rect.y, 60, EditorGUIUtility.singleLineHeight), "Delay", EditorStyles.centeredGreyMiniLabel);
            EditorGUI.LabelField(new Rect(rect.x + 110, rect.y, 60, EditorGUIUtility.singleLineHeight), "Duration", EditorStyles.centeredGreyMiniLabel);
            EditorGUI.LabelField(new Rect(rect.x + 180, rect.y, rect.width - 195, EditorGUIUtility.singleLineHeight), "Text", EditorStyles.centeredGreyMiniLabel);
        }

        private void DrawReorderableSequenceElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            SerializedProperty curProperty = reorderableElementsList.serializedProperty.GetArrayElementAtIndex(index);

            rect.y += 2;
            EditorGUI.LabelField(new Rect(rect.x, rect.y, 30, EditorGUIUtility.singleLineHeight), index.ToString());
            EditorGUI.PropertyField(new Rect(rect.x + 35, rect.y, 60, EditorGUIUtility.singleLineHeight), curProperty.FindPropertyRelative("startDelay"), GUIContent.none);
            EditorGUI.PropertyField(new Rect(rect.x + 100, rect.y, 60, EditorGUIUtility.singleLineHeight), curProperty.FindPropertyRelative("duration"), GUIContent.none);
            SerializedProperty linesProperty = curProperty.FindPropertyRelative("lines");
            for (int i = 0; i < linesProperty.arraySize; i++)
            {
                EditorGUI.PropertyField(new Rect(rect.x + 170, rect.y, rect.width - 195, EditorGUIUtility.singleLineHeight), linesProperty.GetArrayElementAtIndex(i), GUIContent.none);
                GUI.color = Color.red;
                if (GUI.Button(new Rect(rect.width + 13, rect.y, 20, EditorGUIUtility.singleLineHeight), "X"))
                    linesProperty.DeleteArrayElementAtIndex(i);
                GUI.color = Color.white;
                rect.y += 20;
                rect.height += 20;
            }

            GUI.color = Color.green;
            if (GUI.Button(new Rect(rect.width + 13, rect.y, 20, EditorGUIUtility.singleLineHeight), "+"))
                linesProperty.InsertArrayElementAtIndex(linesProperty.arraySize);
            GUI.color = Color.white;

        }

        private float ElementHeight(int _index)
        {
            SerializedProperty curProperty = reorderableElementsList.serializedProperty.GetArrayElementAtIndex(_index);
            SerializedProperty linesProperty = curProperty.FindPropertyRelative("lines");
            return linesProperty.arraySize * 20f + 20f;
        }

        public void AddSequenceToReorderableList(ReorderableList list)
        {
            list.serializedProperty.InsertArrayElementAtIndex(list.serializedProperty.arraySize);
            list.serializedProperty.GetArrayElementAtIndex(list.serializedProperty.arraySize - 1).FindPropertyRelative("startDelay").floatValue = 0;
            list.serializedProperty.GetArrayElementAtIndex(list.serializedProperty.arraySize - 1).FindPropertyRelative("duration").floatValue = 1;
            list.serializedProperty.GetArrayElementAtIndex(list.serializedProperty.arraySize - 1).FindPropertyRelative("lines").ClearArray();
            list.serializedProperty.GetArrayElementAtIndex(list.serializedProperty.arraySize - 1).FindPropertyRelative("lines").InsertArrayElementAtIndex(0);
            list.serializedProperty.GetArrayElementAtIndex(list.serializedProperty.arraySize - 1).FindPropertyRelative("lines").GetArrayElementAtIndex(0).stringValue = "Default";
        }

        private void ImportFromSRT()
        {
            string path = EditorUtility.OpenFilePanel("Import " + target.name + " from SRT", "", "srt");
            if (path.Length != 0)
            {
                if (!File.Exists(path))
                {
                    Debug.LogError("File doesnt exist!");
                    return;
                }
                string fileContent = File.ReadAllText(path, System.Text.Encoding.UTF8);
                ((SubtitleSequence)serializedObject.targetObject).LoadFromSRT(fileContent);
            }
        }

        private void ExportToSRT(string _extention)
        {
            SubtitleSequence selectedSequence = ((SubtitleSequence)serializedObject.targetObject);

            string path = EditorUtility.SaveFilePanel("Export to SRT", "", selectedSequence.name, _extention);
            if (path.Length != 0)
            {
                string finalText = selectedSequence.ExportToSRT();
                FileStream srtStream = File.Open(path, FileMode.OpenOrCreate);
                srtStream.Write(System.Text.UnicodeEncoding.UTF8.GetBytes(finalText), 0, System.Text.UnicodeEncoding.UTF8.GetByteCount(finalText));
                srtStream.Close();
            }
        }

    }
}