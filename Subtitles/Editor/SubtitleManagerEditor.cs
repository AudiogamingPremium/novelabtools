﻿namespace Novelab.Subtitles
{
    using UnityEngine;
    using UnityEditor;
    using UnityEditorInternal;

    [CustomEditor(typeof(SubtitleManager))]
    public class SubtitleManagerEditor : Editor
    {
        private ReorderableList reorderableSubtitleSequencesList;

        private SerializedProperty dbActive;

        private void OnEnable()
        {
            reorderableSubtitleSequencesList = new ReorderableList(serializedObject, serializedObject.FindProperty("subtitleSequencesList"), true, true, true, true);
            reorderableSubtitleSequencesList.drawElementCallback = DrawReorderableSequenceElement;
            reorderableSubtitleSequencesList.onReorderCallback = ReorderSequences;
            reorderableSubtitleSequencesList.onAddCallback = AddSequenceToReorderableList;

            dbActive = serializedObject.FindProperty("dbActive");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Debug", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(dbActive);

            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Subtitle sequences", EditorStyles.boldLabel);
            reorderableSubtitleSequencesList.DoLayoutList();

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawReorderableSequenceElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            SerializedProperty curProperty = reorderableSubtitleSequencesList.serializedProperty.GetArrayElementAtIndex(index);

            rect.y += 2;
            EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width - 60, EditorGUIUtility.singleLineHeight), index.ToString());
            EditorGUI.PropertyField(new Rect(rect.x + 60, rect.y, rect.width - 60, EditorGUIUtility.singleLineHeight), curProperty, GUIContent.none);
        }

        public void AddSequenceToReorderableList(ReorderableList list)
        {
            list.serializedProperty.InsertArrayElementAtIndex(list.serializedProperty.arraySize);

            GameObject newSequenceGO = new GameObject((list.serializedProperty.arraySize - 1).ToString("00") + "_SubTitleSequence");
            newSequenceGO.transform.SetParent(((SubtitleManager)serializedObject.targetObject).transform);
            SubtitleSequence newSequence = newSequenceGO.AddComponent<SubtitleSequence>();
            list.serializedProperty.GetArrayElementAtIndex(list.serializedProperty.arraySize - 1).objectReferenceValue = newSequence;
        }

        public void ReorderSequences(ReorderableList list)
        {
            SubtitleSequence tempSeq;
            string[] split;
            // On parcours tous les éléments et on les renomme / repositionne
            for (int i = 0; i < list.serializedProperty.arraySize; i++)
            {
                tempSeq = (SubtitleSequence)list.serializedProperty.GetArrayElementAtIndex(i).objectReferenceValue;
                tempSeq.transform.SetSiblingIndex(i);
                split = tempSeq.name.Split('_');
                if (split.Length == 1)
                    tempSeq.name = i.ToString("00") + "_" + tempSeq.name;
                else
                    tempSeq.name = i.ToString("00") + "_" + tempSeq.name.Substring(split[0].Length + 1);
            }

        }
    }
}