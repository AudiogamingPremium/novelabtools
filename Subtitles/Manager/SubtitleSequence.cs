﻿namespace Novelab.Subtitles
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public class SubtitleElement
    {
        [SerializeField]
        private float startDelay;
        public float StartDelay
        { get { return startDelay; } }
        [SerializeField]
        private float duration;
        public float Duration
        { get { return duration; } }
        [SerializeField]
        private List<string> lines;
        public List<string> Lines
        { get { return lines; } }

        public SubtitleElement(float _startDelay, float _duration, List<string> _lines)
        {
            startDelay = _startDelay;
            duration = _duration;
            lines = new List<string>(_lines);
        }
    }

    public class SubtitleSequence : MonoBehaviour
    {

        [SerializeField]
        private List<SubtitleElement> subtitleElementsList = new List<SubtitleElement>();

        public void LoadSRTFromTextAsset(string _language)
        {
            TextAsset tradSRT = Resources.Load<TextAsset>("Subtitles_" + _language + "/" + name);

            if (tradSRT == null)
            {
                Debug.LogError("Subtitles_" + _language + "/" + name + " file doesn't exist");
                return;
            }

            LoadFromSRT(tradSRT.text);
        }

        public void LoadFromSRT(string _fileContent)
        {
            List<SRTUnit> SRTUnits = SRTParser.ParseFileContent(_fileContent);

            subtitleElementsList.Clear();
            float delay = 0, duration = 0;

            for (int i = 0; i < SRTUnits.Count; i++)
            {
                if (i == 0)
                    delay = SRTUnits[i].StartTime;
                else
                    delay = SRTUnits[i].StartTime - SRTUnits[i - 1].EndTime;

                duration = SRTUnits[i].EndTime - SRTUnits[i].StartTime;

                subtitleElementsList.Add(new SubtitleElement(delay, duration, SRTUnits[i].Lines));
            }
        }

        public string ExportToSRT()
        {
            string finalContent = "";

            System.TimeSpan currentTime = System.TimeSpan.FromSeconds(0);

            for (int i = 0; i < subtitleElementsList.Count; i++)
            {
                finalContent += "\n" + (i + 1);
                currentTime = currentTime.Add(System.TimeSpan.FromSeconds(subtitleElementsList[i].StartDelay));
                finalContent += "\n" + currentTime.Hours.ToString("00") + ":" + currentTime.Minutes.ToString("00") + ":" + currentTime.Seconds.ToString("00") + "," + currentTime.Milliseconds.ToString("000");
                currentTime = currentTime.Add(System.TimeSpan.FromSeconds(subtitleElementsList[i].Duration));
                finalContent += " --> " + currentTime.Hours.ToString("00") + ":" + currentTime.Minutes.ToString("00") + ":" + currentTime.Seconds.ToString("00") + "," + currentTime.Milliseconds.ToString("000");

                for (int y = 0; y < subtitleElementsList[i].Lines.Count; y++)
                {
                    finalContent += "\n" + subtitleElementsList[i].Lines[y];
                }
                finalContent += "\n";
            }

            return finalContent;
        }

        public SubtitleElement GetElementAtIndex(int _index)
        {
            if (_index < subtitleElementsList.Count)
                return subtitleElementsList[_index];
            else
            {
                Debug.LogError("Out of bounds for " + name + " index " + _index);
                return null;
            }
        }

        public int GetElementsCount()
        {
            return subtitleElementsList.Count;
        }
        
    }
}