﻿namespace Novelab.Subtitles
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class SubtitleManager : MonoBehaviour
    {
        [SerializeField]
        private bool dbActive = false;
        private void DebugLog(string _log)
        {
            if (dbActive)
                Debug.Log("[SubTitleManager] " + _log);
        }

        [SerializeField]
        private List<SubtitleSequence> subtitleSequencesList = new List<SubtitleSequence>();

        private bool pause = false;
        private Coroutine readingCoroutine;

        public delegate void StringDelegate(string _text);
        private StringDelegate OnTextChange;
        public delegate void VoidDelegate();
        private VoidDelegate OnTextHide;


        public void StartReadingSubtitleSequence(int _sequenceId, int _startElementId = 0)
        {
            StopReading();

            DebugLog("StartReadingSubtitleSequence ID : " + _sequenceId + " - Element : " + _startElementId);

            readingCoroutine = StartCoroutine(ReadingSubtitlesCoroutine(_sequenceId, _startElementId));
        }

        public void StopReading()
        {
            DebugLog("StopReading");
            if (readingCoroutine != null)
                StopCoroutine(readingCoroutine);

            TrigOnTextHide();
        }

        private IEnumerator ReadingSubtitlesCoroutine(int _sequenceId, int _startElementId)
        {
            float elapsedTime = 0;
            bool ended = false;
            int totalElements = subtitleSequencesList[_sequenceId].GetElementsCount();

            int currentElementId = _startElementId;
            SubtitleElement currentElement = subtitleSequencesList[_sequenceId].GetElementAtIndex(currentElementId);
            bool waitToRead = true;

            while (!ended)
            {
                if (pause)
                {
                    yield return null;
                    continue;
                }

                if (waitToRead && elapsedTime >= currentElement.StartDelay)
                {
                    // On affiche le texte
                    TrigOnTextChange(currentElement);
                    waitToRead = false;
                }
                if (elapsedTime >= currentElement.StartDelay + currentElement.Duration)
                {
                    // On masque le texte
                    TrigOnTextHide();

                    // On réinitialise le temps écoulé en fonction du temps passé sur ce sous-titre
                    elapsedTime -= currentElement.StartDelay + currentElement.Duration;

                    // On passe à l'élément suivant
                    currentElementId++;

                    // S'il n'y en a plus, on stoppe la coroutine
                    if (currentElementId >= totalElements)
                    {
                        ended = true;
                        continue;
                    }

                    // On récupère le nouvel élément
                    currentElement = subtitleSequencesList[_sequenceId].GetElementAtIndex(currentElementId);

                    // On réinitialise les variables
                    waitToRead = true;
                }

                yield return null;
                elapsedTime += Time.deltaTime;
            }

        }

        private void TrigOnTextChange(SubtitleElement _triggedElement)
        {
            DebugLog("TrigOnTextChange : " + GetStringFromSubTitleElement(_triggedElement));
            if (OnTextChange != null)
                OnTextChange(GetStringFromSubTitleElement(_triggedElement));
        }

        private void TrigOnTextHide()
        {
            DebugLog("TrigOnTextHide");
            if (OnTextHide != null)
                OnTextHide();
        }

        private string GetStringFromSubTitleElement(SubtitleElement _triggedElement)
        {
            string myString = _triggedElement.Lines[0];

            for (int i = 1; i < _triggedElement.Lines.Count; i++)
            {
                myString += "\n" + _triggedElement.Lines[i];
            }

            return myString;
        }

        public void Pause()
        {
            DebugLog("Pause");
            pause = true;
        }

        public void Resume()
        {
            DebugLog("Resume");
            pause = false;
        }

        public void RegisterElement(StringDelegate _textChangeCallback, VoidDelegate _textHideCallback)
        {
            OnTextChange += _textChangeCallback;
            OnTextHide += _textHideCallback;
        }

        public void UnregisterElement(StringDelegate _textChangeCallback, VoidDelegate _textHideCallback)
        {
            OnTextChange -= _textChangeCallback;
            OnTextHide -= _textHideCallback;
        }

        public void LoadLanguageFiles(string _language)
        {
            DebugLog("LoadLanguageFiles : " + _language);
            foreach (SubtitleSequence sequence in subtitleSequencesList)
            {
                sequence.LoadSRTFromTextAsset(_language);
            }
        }

    }
}