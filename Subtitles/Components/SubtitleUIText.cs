﻿namespace Novelab.Subtitles
{
    using UnityEngine;
    using UnityEngine.UI;
    using Novelab.Fade;

    [RequireComponent(typeof(Text))]
    [RequireComponent(typeof(FadeInOutUIText))]
    public class SubtitleUIText : MonoBehaviour
    {
        [SerializeField]
        private SubtitleManager subtitleManager;
        private Text uiText;
        private FadeInOutUIText fadeText;

        private void Awake()
        {
            uiText = GetComponent<Text>();
            fadeText = GetComponent<FadeInOutUIText>();
        }

        private void Start()
        {
            fadeText.SetAlpha(0);
        }

        private void OnEnable()
        {
            subtitleManager.RegisterElement(TextChange, TextHide);
        }

        private void OnDisable()
        {
            subtitleManager.UnregisterElement(TextChange, TextHide);
        }

        private void TextChange(string _text)
        {
            uiText.text = _text;
            fadeText.FadeIn();
        }

        private void TextHide()
        {
            fadeText.FadeOut();
        }

    }
}