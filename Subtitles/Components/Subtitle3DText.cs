﻿namespace Novelab.Subtitles
{
    using UnityEngine;
    using Novelab.Fade;

    [RequireComponent(typeof(TextMesh))]
    [RequireComponent(typeof(FadeInOut3DText))]
    public class Subtitle3DText : MonoBehaviour
    {
        [SerializeField]
        private SubtitleManager subtitleManager;
        private TextMesh myTextMesh;
        private FadeInOut3DText fade3DText;

        private void Awake()
        {
            myTextMesh = GetComponent<TextMesh>();
            fade3DText = GetComponent<FadeInOut3DText>();
        }

        private void Start()
        {
            fade3DText.SetAlpha(0);
        }

        private void OnEnable()
        {
            subtitleManager.RegisterElement(TextChange, TextHide);
        }

        private void OnDisable()
        {
            subtitleManager.UnregisterElement(TextChange, TextHide);
        }

        private void TextChange(string _text)
        {
            myTextMesh.text = _text;
            fade3DText.FadeIn();
        }

        private void TextHide()
        {
            fade3DText.FadeOut();
        }
    }
}