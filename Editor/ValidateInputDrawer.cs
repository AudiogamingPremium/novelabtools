﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using UnityEditor;
using UnityEngine;

#if UNITY_2017_3_OR_NEWER
namespace Novelab.Editor
{
    [CustomPropertyDrawer(typeof(ValidateInputAttribute))]
    public class ValidateInputDrawer : PropertyDrawer
    {
        // Caching values
        Type type;
        ValidateInputAttribute validateInputAttribute;
        MethodInfo validationMethod;
        bool dontValidate = false;
        bool valuesAreCached = false;
        bool waitedAFrame = false;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginChangeCheck();
            position.height = EditorGUIUtility.singleLineHeight * 2;

            if (!valuesAreCached)
            {
                CacheValues(property);
                valuesAreCached = true;
            }

            if (IsArray(property))
            {
                if (dontValidate)
                {
                    DrawListSimple(position, property);
                }
                else if (validationMethod == null)
                {
                    if (IsFirstElementOfArray(property))
                    {
                        EditorGUI.HelpBox(position, SceneValidation.sceneValidationPrefix + " Method does not exists : " + validateInputAttribute.ValidationFunction, MessageType.Error);
                    }
                }
                else if (validationMethod.ReturnType != typeof(bool))
                {
                    if (IsFirstElementOfArray(property))
                        EditorGUI.HelpBox(position, SceneValidation.sceneValidationPrefix + " Method return type is not bool : [" + validateInputAttribute.ValidationFunction + "] is returning " + validationMethod.ReturnType, MessageType.Error);
                }
                else
                {
                    DrawList(position, property);
                }
            }
            else
            {
                if (dontValidate)
                {
                    DrawSingleOrStructSimple(position, property);
                }
                else if (validationMethod == null)
                {
                    EditorGUI.HelpBox(position, SceneValidation.sceneValidationPrefix + " Method does not exists : " + validateInputAttribute.ValidationFunction, MessageType.Error);
                }
                else if (validationMethod.ReturnType != typeof(bool))
                {
                    EditorGUI.HelpBox(position, SceneValidation.sceneValidationPrefix + " Method return type is not bool : [" + validateInputAttribute.ValidationFunction + "] is returning " + validationMethod.ReturnType, MessageType.Error);
                }
                else
                {
                    DrawSingleOrStruct(position, property);
                }
            }
            if (EditorGUI.EndChangeCheck())
            {
                waitedAFrame = true;
            }
            else if (waitedAFrame)
            {
                SceneValidation.CallbackCall(property, validateInputAttribute);
                waitedAFrame = false;
            }
        }
        private void DrawSingleOrStruct(Rect position, SerializedProperty property)
        {
            if (!SceneValidation.IsFieldValid(property, validateInputAttribute))
            {
                if (validateInputAttribute.ValidationMessage != "")
                    EditorGUI.HelpBox(position, validateInputAttribute.ValidationMessage, validateInputAttribute.ValidationMessageType);
                else
                    EditorGUI.HelpBox(position, validateInputAttribute.ValidationFunction + "(" + property.name + ") = false", validateInputAttribute.ValidationMessageType);
                position.yMin += EditorGUIUtility.singleLineHeight * 2;
                position.yMax += EditorGUIUtility.singleLineHeight * 2;
                position.height = EditorGUIUtility.singleLineHeight;
            }
            else
            {
                position.height = EditorGUIUtility.singleLineHeight;
                float previousxMin = position.xMin;
                position.xMin = EditorGUIUtility.labelWidth;
                GUI.Label(position, EditorGUIUtility.IconContent("Collab"));
                position.xMin = previousxMin;
            }
            // Structs
            if (property.hasVisibleChildren)
            {
                EditorGUI.PropertyField(position, property, true);
            }
            else
                EditorGUI.PropertyField(position, property, false);
        }
        private void DrawList(Rect position, SerializedProperty property)
        {
            if (IsFirstElementOfArray(property))
            {
                if (!SceneValidation.IsFieldValid(property, validateInputAttribute))
                {
                    if (validateInputAttribute.ValidationMessage != "")
                        EditorGUI.HelpBox(position, validateInputAttribute.ValidationMessage, validateInputAttribute.ValidationMessageType);
                    else
                        EditorGUI.HelpBox(position, validateInputAttribute.ValidationFunction + "(" + property.name + ") = false", validateInputAttribute.ValidationMessageType);
                    position.yMin += EditorGUIUtility.singleLineHeight * 2;
                    position.yMax += EditorGUIUtility.singleLineHeight * 2;
                }
                else
                {
                    position.y -= EditorGUIUtility.singleLineHeight;
                    GUI.Label(position, EditorGUIUtility.IconContent("Collab"));
                    position.y += EditorGUIUtility.singleLineHeight;
                }
            }
            EditorGUI.PropertyField(position, property, true);
        }
        private void DrawListSimple(Rect position, SerializedProperty property)
        {
            EditorGUI.PropertyField(position, property, true);
        }
        private void DrawSingleOrStructSimple(Rect position, SerializedProperty property)
        {
            EditorGUI.PropertyField(position, property, true);
        }
        private void CacheValues(SerializedProperty property)
        {
            type = property.serializedObject.targetObject.GetType();
            validateInputAttribute = (ValidateInputAttribute)attribute;
            if (validateInputAttribute.ValidationFunction == "")
                dontValidate = true;
            else
                validationMethod = type.GetMethod(validateInputAttribute.ValidationFunction, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static);
        }
        private bool IsArray(SerializedProperty property)
        {
            string[] splitProperty = property.propertyPath.Split('.');
            return splitProperty.Length > 0 && property.serializedObject.FindProperty(splitProperty[0]).isArray;
        }
        private bool IsFirstElementOfArray(SerializedProperty property)
        {
            string[] splitProperty = property.propertyPath.Split('.');
            return IsArray(property) && splitProperty[2] == "data[0]";
        }
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (dontValidate)
            {
                return EditorGUI.GetPropertyHeight(property);
            }
            ValidateInputAttribute validateInputAttribute = (ValidateInputAttribute)attribute;
            if (validationMethod == null || validationMethod.ReturnType != typeof(bool))
            {
                if (IsArray(property))
                {
                    if (IsFirstElementOfArray(property))
                    {
                        return EditorGUIUtility.singleLineHeight * 2;
                    }
                    return 0;
                }
                return EditorGUIUtility.singleLineHeight * 2;
            }
            if (IsArray(property))
            {
                if (IsFirstElementOfArray(property))
                {
                    return SceneValidation.IsFieldValid(property, validateInputAttribute) ? EditorGUI.GetPropertyHeight(property) : EditorGUI.GetPropertyHeight(property) + EditorGUIUtility.singleLineHeight * 2;
                }
                else
                {
                    return EditorGUI.GetPropertyHeight(property);
                }
            }
            else
            {
                return SceneValidation.IsFieldValid(property, validateInputAttribute) ? EditorGUI.GetPropertyHeight(property) : EditorGUI.GetPropertyHeight(property) + EditorGUIUtility.singleLineHeight * 2;
            }
        }
        public override bool CanCacheInspectorGUI(SerializedProperty property)
        {
            if (property.hasVisibleChildren)
                return false;
            else
                return true;
        }
    }
}
#endif