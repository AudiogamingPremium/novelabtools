﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_2017_3_OR_NEWER && NET_4_6
namespace Novelab.Editor.Samples
{
    public class ValidationExample : MonoBehaviour
    {
        //Basic
        [ValidateInput(ValidationFunction = nameof(ValidationExample.NotNull))]
        public GameObject myNicePrefab;

        //Works with custom structs
        [ValidateInput(ValidationFunction = nameof(ValidationExample.CustomDataValidate), ValidationMessage = "cutomData.myVector.x doit être multiple de 2 !", ValidationMessageType = UnityEditor.MessageType.Error)]
        public CustomData customData;

        //Different options for message type : Warning, Error, Info
        [ValidateInput(ValidationFunction = nameof(ValidationExample.CustomDataValidate), ValidationMessage = "cutomData.myVector.x doit être multiple de 2 !", ValidationMessageType = UnityEditor.MessageType.Warning)]
        public CustomData otherCustomData;

        //Works with lists and arrays
        [ValidateInput(ValidationFunction = nameof(ValidationExample.MustNotContainNulls), ValidationMessage = "List must not contain nulls !", ValidationMessageType = UnityEditor.MessageType.Error)]
        public GameObject[] gameObjects = new GameObject[10];

        //You can do this
        [ValidateInput(ValidationFunction = nameof(ValidationExample.MustHaveRigidbody), ValidationMessage = "The gameobject must have a rigidbody", ValidationMessageType = UnityEditor.MessageType.Error, Callback = nameof(ValidationExample.SetRigidBody))]
        public GameObject myOtherPrefab;
        public Rigidbody myOtherPrefabRigidbody;
        [ValidateInput(Callback = nameof(ValidationExample.AutoNum))]
        public string[] autoNum;
        [ValidateInput(Callback = nameof(ValidationExample.AutoNumG))]
        public GameObject[] autoNumGameobject;
        // Properties are checked before play but not displayed
        [ValidateInput(ValidationFunction = "SupZero")]
        public int ListSize
        {
            get
            {
                return gameObjects.Length;
            }
        }
        [ValidateInput(Callback = nameof(ValidationExample.Link))]
        public int firstField;
        public float linkedField;
        // You can place editor-only checks inside UNITY_EDITOR directive so it won't be included in builds
#if UNITY_EDITOR
        private void AutoNum(string[] s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i].Length == 0 || s[i].Length >= 0 && s[i][0] !='[')
                    s[i] = "[" + i + "]" + " " + s[i];
                else if(s[i].Length >= 4 && s[i][0] == '[')
                {
                    s[i] =  "["+i+"] "+ s[i].Remove(0, 3+i.ToString().Length);
                }
            }
        }
        private void AutoNumG(GameObject[] g)
        {
            for (int i = 0; i < g.Length; i++)
            {
                if (!g[i])
                    continue;
                if (g[i].name.Length == 0)
                {
                    g[i].name = "[" + i + "] " + g[i].name;
                }
                else if (g[i].name[0] == '[')
                {
                    g[i].name = "[" + i + "] " + g[i].name.Remove(0, 3+i.ToString().Length);
                }
                else
                {
                    g[i].name = "[" + i + "] " + g[i].name;
                }
            }
        }
        private void Link(int first)
        {
            myOtherPrefab.name = "MyFabulousPrefab " + (first * 2).ToString();
        }
        private void SetRigidBody(GameObject gameObject)
        {
            if (gameObject && gameObject.GetComponent<Rigidbody>())
            {
                myOtherPrefabRigidbody = gameObject.GetComponent<Rigidbody>();
            }
        }
        private bool MustNotContainNulls()
        {
            return Array.FindAll(gameObjects, (g) => g == null).Length == 0;
        }
        private bool MustHaveRigidbody(GameObject gameObject)
        {
            return gameObject && gameObject.GetComponent<Rigidbody>();
        }
        public bool SupZero(float tested)
        {
            return tested > 0;
        }
        public bool CustomDataValidate(CustomData cutomData)
        {
            return cutomData.myVector.x % 2 == 0;
        }
        private bool NotNull(GameObject o)
        {
            return o != null;
        }
#endif

    }
    [System.Serializable]
    public struct CustomData
    {
        public Vector3 myVector;
        public int myInt;
        public float myNiceFloat;
    }
}
#endif