﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_2017_3_OR_NEWER
namespace Novelab.Editor
{
    [AttributeUsageAttribute((AttributeTargets)384, AllowMultiple = false, Inherited = true)]
    public sealed class ValidateInputAttribute : PropertyAttribute
    {
        public string ValidationFunction = "";
        public string Callback = "";
        public string ValidationMessage = "";
        public MessageType ValidationMessageType = MessageType.Error;
        public ValidateInputAttribute()
        {

        }
    }
}
#endif