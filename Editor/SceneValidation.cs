﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

#if UNITY_2017_3_OR_NEWER
namespace Novelab.Editor
{
    public class SceneValidation : EditorWindow
    {
        // Caching
        static List<AttributeRef> attributeRefs = new List<AttributeRef>();
        static Dictionary<AttributeRef, bool> foldouts = new Dictionary<AttributeRef, bool>();
        public static string sceneValidationPrefix = "[SceneValidation]";

        // Menu
        static bool infoToggle = false;
        static bool warningToggle = false;
        static bool errorToggle = true;
        List<MessageType> SelectedMessageTypes = new List<MessageType>();

        //Styles
        static GUIStyle dropDownStyle = new GUIStyle();

        [MenuItem("Novelab/Scene Validation")]
        public static void ShowWindow()
        {
            infoToggle = EditorPrefs.GetBool(sceneValidationPrefix + "infoToggle", infoToggle);
            warningToggle = EditorPrefs.GetBool(sceneValidationPrefix + "warningToggle", warningToggle);
            errorToggle = EditorPrefs.GetBool(sceneValidationPrefix + "errorToggle", errorToggle);
            dropDownStyle = new GUIStyle(EditorStyles.helpBox);
            Texture2D tex = new Texture2D(1, 1);
            tex.SetPixels(new Color[] { new Color(1, 0.8f, 0.8f) });
            tex.Apply();
            dropDownStyle.normal.background = tex;
            EditorWindow.GetWindow(typeof(SceneValidation));
            UpdateFields();
        }
        private void OnDestroy()
        {
            EditorPrefs.SetBool(sceneValidationPrefix + "infoToggle", infoToggle);
            EditorPrefs.SetBool(sceneValidationPrefix + "warningToggle", warningToggle);
            EditorPrefs.SetBool(sceneValidationPrefix + "errorToggle", errorToggle);
        }
        void OnGUI()
        {
            // Menu
            GUILayout.BeginHorizontal(EditorStyles.toolbar);
            infoToggle = GUILayout.Toggle(infoToggle, EditorGUIUtility.FindTexture("d_console.infoicon.sml"), EditorStyles.toolbarButton);
            warningToggle = GUILayout.Toggle(warningToggle, EditorGUIUtility.FindTexture("d_console.warnicon.sml"), EditorStyles.toolbarButton);
            errorToggle = GUILayout.Toggle(errorToggle, EditorGUIUtility.FindTexture("d_console.erroricon.sml"), EditorStyles.toolbarButton);
            if (GUILayout.Button("Update") || attributeRefs.FindAll((a) => a.unityObject == null).Count > 0)
            {
                UpdateFields();
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            DrawFields();
        }
        void DrawFields()
        {
            SelectedMessageTypes.Clear();
            if (infoToggle)
                SelectedMessageTypes.Add(MessageType.Info);
            if (warningToggle)
                SelectedMessageTypes.Add(MessageType.Warning);
            if (errorToggle)
                SelectedMessageTypes.Add(MessageType.Error);

            foreach (AttributeRef item in attributeRefs)
            {
                if (SelectedMessageTypes.Contains(item.validateInputAttribute.ValidationMessageType))
                {
                    if (item.isValid)
                    {
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(EditorGUIUtility.IconContent("Collab"));
                        EditorGUILayout.LabelField("[" + item.unityObject.name + "]" + item.Name);
                        GUILayout.FlexibleSpace();
                        EditorGUILayout.EndHorizontal();
                    }
                    else
                    {
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(EditorGUIUtility.IconContent("CollabConflict"));
                        foldouts[item] = EditorGUILayout.Foldout(foldouts[item], "[" + item.unityObject.name + "]" + item.Name);
                        GUILayout.FlexibleSpace();
                        EditorGUILayout.EndHorizontal();
                        if (foldouts[item])
                        {
                            EditorGUILayout.BeginVertical(dropDownStyle);
                            UnityEditor.Editor inlineEditor = UnityEditor.Editor.CreateEditor(item.unityObject);
                            if (inlineEditor)
                                inlineEditor.OnInspectorGUI();
                            EditorGUILayout.EndVertical();
                            GUI.backgroundColor = Color.white;
                        }
                    }
                }
            }
        }
        public static void UpdateFields()
        {
            attributeRefs.Clear();
            foldouts.Clear();
            UnityEngine.Object[] objects = FindObjectsOfType<UnityEngine.Object>();

            foreach (var unityObject in objects)
            {
                foreach (var field in unityObject.GetType().GetFields())
                {
                    foreach (var attr in field.GetCustomAttributes(true))
                    {
                        if (attr.GetType() == typeof(ValidateInputAttribute))
                        {
                            ValidateInputAttribute validateInputAttribute = attr as ValidateInputAttribute;
                            if (validateInputAttribute.ValidationFunction != "")
                            {
                                bool isValid = IsFieldValid(unityObject, field, validateInputAttribute);
                                AttributeRef attributeRef = new AttributeRef() { fieldInfo = field, unityObject = unityObject, validateInputAttribute = validateInputAttribute, isValid = isValid };
                                attributeRefs.Add(new AttributeRef() { fieldInfo = field, unityObject = unityObject, validateInputAttribute = validateInputAttribute, isValid = isValid });
                                foldouts.Add(attributeRef, false);
                            }
                        }
                    }
                }
                foreach (var prop in unityObject.GetType().GetProperties())
                {
                    foreach (var attr in prop.GetCustomAttributes(true))
                    {
                        if (attr.GetType() == typeof(ValidateInputAttribute))
                        {
                            ValidateInputAttribute validateInputAttribute = attr as ValidateInputAttribute;
                            if (validateInputAttribute.ValidationFunction != "")
                            {
                                bool isValid = IsFieldValid(unityObject, prop, validateInputAttribute);
                                AttributeRef attributeRef = new AttributeRef() { propertyInfo = prop, unityObject = unityObject, validateInputAttribute = validateInputAttribute, isValid = isValid };
                                attributeRefs.Add(new AttributeRef() { propertyInfo = prop, unityObject = unityObject, validateInputAttribute = validateInputAttribute, isValid = isValid });
                                foldouts.Add(attributeRef, false);
                            }
                        }
                    }
                }
            }
        }
        public static bool AllFieldsValid(MessageType messageType)
        {
            UpdateFields();
            return attributeRefs.FindAll((attr) => attr.validateInputAttribute.ValidationMessageType == messageType).All((attr) => attr.isValid);
        }
        public static bool IsFieldValid(SerializedProperty property, ValidateInputAttribute validateInputAttribute)
        {
            UnityEngine.Object unityObject = property.serializedObject.targetObject;
            var targetObjectClassType = unityObject.GetType();
            FieldInfo fieldInfo;
            // If it's an array
            if (property.propertyPath.Split('.').Length > 1)
            {
                fieldInfo = targetObjectClassType.GetField(property.propertyPath.Split('.')[0]);
            }
            else
            {
                fieldInfo = targetObjectClassType.GetField(property.propertyPath);
            }
            return IsFieldValid(unityObject, fieldInfo, validateInputAttribute);
        }
        public static bool IsFieldValid(UnityEngine.Object unityObject, FieldInfo field, ValidateInputAttribute validateInputAttribute)
        {
            MethodInfo methodInfo = unityObject.GetType().GetMethod(validateInputAttribute.ValidationFunction, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static);
            if (methodInfo != null && methodInfo.ReturnType == typeof(bool))
            {
                if (methodInfo.GetParameters().Length == 0)
                {
                    return (bool)methodInfo.Invoke(unityObject, new object[] { });
                }
                else
                {
                    return (bool)methodInfo.Invoke(unityObject, new object[] { field.GetValue(unityObject) });
                }
            }
            else
                return false;
        }
        public static bool IsFieldValid(UnityEngine.Object unityObject, PropertyInfo prop, ValidateInputAttribute validateInputAttribute)
        {
            MethodInfo methodInfo = unityObject.GetType().GetMethod(validateInputAttribute.ValidationFunction, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static);
            if (methodInfo != null && methodInfo.ReturnType == typeof(bool))
            {
                if (methodInfo.GetParameters().Length == 0)
                {
                    return (bool)methodInfo.Invoke(unityObject, new object[] { });
                }
                else
                {
#if UNITY_2017_1_OR_NEWER
                    return (bool)methodInfo.Invoke(unityObject, new object[] { prop.GetValue(unityObject,null) });
#else
                    return (bool)methodInfo.Invoke(unityObject, new object[] { prop.GetValue(unityObject) });
#endif
                }
            }
            else
                return false;
        }
        public static void CallbackCall(SerializedProperty property, ValidateInputAttribute validateInputAttribute)
        {
            UnityEngine.Object unityObject = property.serializedObject.targetObject;
            var targetObjectClassType = unityObject.GetType();
            FieldInfo fieldInfo;
            // If it's an array
            if (property.propertyPath.Split('.').Length > 1)
            {
                fieldInfo = targetObjectClassType.GetField(property.propertyPath.Split('.')[0]);
            }
            else
            {
                fieldInfo = targetObjectClassType.GetField(property.propertyPath);
            }
            MethodInfo methodInfo = unityObject.GetType().GetMethod(validateInputAttribute.Callback, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static);
            if (methodInfo != null)
            {
                if (methodInfo.GetParameters().Length == 0)
                {
                    methodInfo.Invoke(unityObject, new object[] { });
                }
                else
                {
                    methodInfo.Invoke(unityObject, new object[] { fieldInfo.GetValue(unityObject) });
                }
            }
        }
        struct AttributeRef
        {
            public UnityEngine.Object unityObject;
            public FieldInfo fieldInfo;
            public PropertyInfo propertyInfo;
            public bool isValid;
            public ValidateInputAttribute validateInputAttribute;
            public string Name
            {
                get
                {
                    if (fieldInfo != null)
                        return fieldInfo.Name;
                    else
                        return propertyInfo.Name;
                }
            }
        }
    }
    public class SceneValidationGuard
    {
        [InitializeOnLoadMethod]
        static void Start()
        {
            EditorApplication.playModeStateChanged += HandleOnPlayModeChanged;
        }

        static void HandleOnPlayModeChanged(PlayModeStateChange currentMode)
        {
            if (currentMode == PlayModeStateChange.ExitingEditMode)
            {
                if (!SceneValidation.AllFieldsValid(MessageType.Error))
                {
                    EditorApplication.isPlaying = false;
                    EditorWindow.GetWindow(typeof(SceneValidation));
                }
            }
        }
    }
}
#endif