﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Novelab.Animations
{

    public class AnimationEventsTrigger : MonoBehaviour
    {
        [System.Serializable]
        public struct AnimationEvent
        {
            public string name;
            public UnityEvent animEvent;
        }

        [SerializeField]
        private List<AnimationEvent> m_animEvents = new List<AnimationEvent>();

        private Dictionary<string, UnityEvent> m_animEventsDictionnary = new Dictionary<string, UnityEvent>();

        private void Awake()
        {
            InitDictionnary();
        }

        private void InitDictionnary()
        {
            for (int i = 0; i < m_animEvents.Count; i++)
            {
                if(m_animEventsDictionnary.ContainsKey(m_animEvents[i].name))
                {
                    Debug.LogError("This event name is already used : " + m_animEvents[i].name);
                    continue;
                }
                else
                {
                    m_animEventsDictionnary.Add(m_animEvents[i].name, m_animEvents[i].animEvent);
                }
            }
        }

        public void TrigEvents(string _name)
        {
            UnityEvent trigEvent = GetUnityEvent(_name);

            if (trigEvent == null)
                return;

            trigEvent.Invoke();
        }

        public UnityEvent GetUnityEvent(string _name)
        {
            if (m_animEventsDictionnary.ContainsKey(_name))
                return m_animEventsDictionnary[_name];
            else
            {
                Debug.LogError("There is no AnimationEvent for : " + _name);
                return null;
            }
        }
    }

}