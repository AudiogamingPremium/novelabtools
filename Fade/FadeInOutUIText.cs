﻿namespace Novelab.Fade
{
    using UnityEngine;
    using UnityEngine.UI;

    [RequireComponent(typeof(Text))]
    public class FadeInOutUIText : FadeInOut
    {
        private Text uiText;

        protected void Awake()
        {
            uiText = GetComponent<Text>();
        }

        public override float GetAlpha()
        {
            return GetColor().a;
        }

        public override void SetAlpha(float _alpha)
        {
            Color tempColor = GetColor();
            tempColor.a = _alpha;
            uiText.color = tempColor;
        }

        private Color GetColor()
        {
            return uiText.color;
        }

    }
}