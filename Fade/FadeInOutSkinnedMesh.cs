﻿namespace Novelab.Fade
{
    using UnityEngine;

    [RequireComponent(typeof(SkinnedMeshRenderer))]
    [System.Serializable]
    public class FadeInOutSkinnedMesh : FadeInOut
    {
        private SkinnedMeshRenderer myRenderer;
        [SerializeField]
        private string colorPropertyName = "_Color";

        protected void Awake()
        {
            myRenderer = GetComponent<SkinnedMeshRenderer>();
        }

        public override float GetAlpha()
        {
            return myRenderer.material.GetColor(colorPropertyName).a;
        }

        public override void SetAlpha(float _alpha)
        {
            Color tempColor;
            foreach (Material mat in myRenderer.materials)
            {
                tempColor = mat.GetColor(colorPropertyName);
                tempColor.a = _alpha;
                mat.SetColor(colorPropertyName, tempColor);
            }
        }


    }
}