﻿namespace Novelab.Fade
{
    using System.Collections;
    using UnityEngine;

    public class FadeInOutSOC : FadeInOut
    {
        public override float GetAlpha()
        {
            return Shader.GetGlobalFloat("_OpacityClipOpacity");
        }

        public override void SetAlpha(float _alpha)
        {
            Shader.SetGlobalFloat("_OpacityClipOpacity", _alpha);
        }
    }
}