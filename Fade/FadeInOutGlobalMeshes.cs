﻿namespace Novelab.Fade
{
    using System.Collections.Generic;
    using UnityEngine;

    public class FadeInOutGlobalMeshes : FadeInOut
    {
        [SerializeField]
        private Transform parentTransform;

        private MeshRenderer[] allRenderers;
        private List<Material> sharedMaterials = new List<Material>();


        private void Awake()
        {
            MeshRenderer[] allRenderers = parentTransform.GetComponentsInChildren<MeshRenderer>();
            sharedMaterials = new List<Material>();

            foreach (MeshRenderer meshRend in allRenderers)
            {
                foreach (Material mat in meshRend.sharedMaterials)
                {
                    if (!sharedMaterials.Contains(mat))
                        sharedMaterials.Add(mat);
                }
            }

            SkinnedMeshRenderer[] skinnedMeshRenderers = parentTransform.GetComponentsInChildren<SkinnedMeshRenderer>();

            foreach (SkinnedMeshRenderer meshRend in skinnedMeshRenderers)
            {
                foreach (Material mat in meshRend.sharedMaterials)
                {
                    if (!sharedMaterials.Contains(mat))
                        sharedMaterials.Add(mat);
                }
            }
        }

        public override void SetAlpha(float _alpha)
        {
            foreach (Material mat in sharedMaterials)
            {
                SetMaterialOpacity(mat, _alpha);
            }
        }

        public override float GetAlpha()
        {
            if (sharedMaterials.Count == 0)
                return 0;

            return sharedMaterials[0].GetFloat("_Opacity");
        }

        private void SetMaterialOpacity(Material _mat, float _alpha)
        {
            if (_mat == null)
                return;

            _mat.SetFloat("_Opacity", _alpha);
        }

    }
}