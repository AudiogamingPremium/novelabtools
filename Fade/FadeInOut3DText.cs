﻿namespace Novelab.Fade
{
    using UnityEngine;
    using UnityEngine.UI;

    [RequireComponent(typeof(TextMesh))]
    public class FadeInOut3DText : FadeInOut
    {
        private TextMesh textMesh;

        protected void Awake()
        {
            textMesh = GetComponent<TextMesh>();
        }

        public override float GetAlpha()
        {
            return GetColor().a;
        }

        public override void SetAlpha(float _alpha)
        {
            Color tempColor = GetColor();
            tempColor.a = _alpha;
            textMesh.color = tempColor;
        }

        private Color GetColor()
        {
            return textMesh.color;
        }

    }
}
