﻿namespace Novelab.Fade
{
    using UnityEngine;

    public class FadeInOutLine : FadeInOut
    {
        private LineRenderer myRenderer;
        [SerializeField]
        private string colorPropertyName = "_Alpha";

        protected void Awake()
        {
            myRenderer = GetComponent<LineRenderer>();
        }

        public override float GetAlpha()
        {
            return myRenderer.material.GetFloat(colorPropertyName);
        }

        public override void SetAlpha(float _alpha)
        {
            myRenderer.material.SetFloat(colorPropertyName, _alpha);
        }

    }
}