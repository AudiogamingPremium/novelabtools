﻿namespace Novelab.Fade
{
    using Novelab.Utils;
    using UnityEngine;

    public abstract class FadeInOut : MonoBehaviour
    {
        [SerializeField]
        protected float fadeInDuration = 2;
        public float FadeInDuration
        {
            get { return fadeInDuration; }
        }
        [SerializeField]
        protected float fadeOutDuration = 2;
        public float FadeOutDuration
        {
            get { return fadeOutDuration; }
        }
        [SerializeField]
        protected float maxAlpha = 1;
        [SerializeField]
        protected float minAlpha = 0;
        [SerializeField]
        protected AnimationCurve fadeCurve = AnimationCurve.Linear(0, 0, 1, 1);

        public UnityEngine.Events.UnityEvent OnEndFadeIn;
        public UnityEngine.Events.UnityEvent OnEndFadeOut;

        protected Coroutine fadeCo;

        public void FadeIn(float _startAlpha = -1, float _specialDuration = -1)
        {
            Stop();
            if (gameObject.activeInHierarchy)
                fadeCo = CoroutinesManager.Instance.StartCurveCoroutine(SetFadeInAlpha, _startAlpha < 0 ? GetAlpha() : _startAlpha, maxAlpha, _specialDuration > 0 ? _specialDuration : fadeOutDuration, fadeCurve);
        }

        public void FadeOut(float _startAlpha = -1, float _specialDuration = -1)
        {
            Stop();
            if (gameObject.activeInHierarchy)
                fadeCo = CoroutinesManager.Instance.StartCurveCoroutine(SetFadeOutAlpha, _startAlpha < 0 ? GetAlpha() : _startAlpha, minAlpha, _specialDuration > 0 ? _specialDuration : fadeOutDuration, fadeCurve);
        }

        private void SetFadeInAlpha(float _alpha)
        {
            SetAlpha(_alpha);

            if (_alpha == maxAlpha)
                TrigEndFadeIn();
        }

        private void SetFadeOutAlpha(float _alpha)
        {
            SetAlpha(_alpha);

            if (_alpha == minAlpha)
                TrigEndFadeOut();
        }

        public abstract void SetAlpha(float _alpha);

        public abstract float GetAlpha();

        public void Stop()
        {
            if (fadeCo != null)
                CoroutinesManager.Instance.StopCoroutine(fadeCo);
        }

        private void TrigEndFadeIn()
        {
            OnEndFadeIn.Invoke();
        }

        private void TrigEndFadeOut()
        {
            OnEndFadeOut.Invoke();
        }

        public void RegisterEndFadeIn(UnityEngine.Events.UnityAction _action)
        {
            OnEndFadeIn.AddListener(_action);
        }

        public void UnregisterEndFadeIn(UnityEngine.Events.UnityAction _action)
        {
            OnEndFadeIn.RemoveListener(_action);
        }

        public void RegisterEndFadeOut(UnityEngine.Events.UnityAction _action)
        {
            OnEndFadeOut.AddListener(_action);
        }

        public void UnregisterEndFadeOut(UnityEngine.Events.UnityAction _action)
        {
            OnEndFadeOut.RemoveListener(_action);
        }

    }
}