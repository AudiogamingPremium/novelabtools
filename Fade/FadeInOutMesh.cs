﻿namespace Novelab.Fade
{
    using System.Collections;
    using UnityEngine;

    [RequireComponent(typeof(MeshRenderer))]
    [System.Serializable]
    public class FadeInOutMesh : FadeInOut
    {
        private MeshRenderer myRenderer;
        [SerializeField]
        private string colorPropertyName = "_Color";

        protected void Awake()
        {
            myRenderer = GetComponent<MeshRenderer>();
        }

        public override float GetAlpha()
        {
            return myRenderer.material.GetColor(colorPropertyName).a;
        }

        public override void SetAlpha(float _alpha)
        {
            Color tempColor;
            foreach (Material mat in myRenderer.materials)
            {
                tempColor = mat.GetColor(colorPropertyName);
                tempColor.a = _alpha;
                mat.SetColor(colorPropertyName, tempColor);
            }

            if (_alpha == 0 && myRenderer.enabled)
                myRenderer.enabled = false;
            else if (!myRenderer.enabled && _alpha > 0)
                myRenderer.enabled = true;
        }
    }
}