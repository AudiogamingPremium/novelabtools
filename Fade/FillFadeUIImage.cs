﻿namespace Novelab.Fade
{
    using UnityEngine;
    using UnityEngine.UI;

    [RequireComponent(typeof(Image))]
    public class FillFadeUIImage : FadeInOut
    {
        private Image uiImage;

        protected void Awake()
        {
            uiImage = GetComponent<Image>();
        }

        public override float GetAlpha()
        {
            return uiImage.fillAmount;
        }

        public override void SetAlpha(float _alpha)
        {
            uiImage.fillAmount = _alpha;
        }
    }
}