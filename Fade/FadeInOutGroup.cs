﻿namespace Novelab.Fade
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class FadeInOutGroup : MonoBehaviour
    {
        [SerializeField]
        private List<FadeInOut> fadeElements = new List<FadeInOut>();

        public delegate void VoidDelegate();
        private VoidDelegate OnEndFadeIn;
        private VoidDelegate OnEndFadeOut;

        private int count = 0;

        private void Start()
        {
            foreach (FadeInOut item in fadeElements)
            {
                item.RegisterEndFadeIn(EndFadeIn);
                item.RegisterEndFadeOut(EndFadeOut);
            }
        }

        public void AddFadeInOutElement(FadeInOut _fadeElement)
        {
            fadeElements.Add(_fadeElement);
        }

        public void RemoveFadeInOutElement(FadeInOut _fadeElement)
        {
            fadeElements.Remove(_fadeElement);
        }

        public void Clear()
        {
            fadeElements.Clear();
        }

        public void FadeIn()
        {
            count = 0;
            foreach (FadeInOut item in fadeElements)
                item.FadeIn();
        }

        public void FadeOut()
        {
            count = 0;
            foreach (FadeInOut item in fadeElements)
                item.FadeOut();
        }

        public void SetAlpha(float _alpha)
        {
            foreach (FadeInOut item in fadeElements)
                item.SetAlpha(_alpha);
        }

        private void EndFadeIn()
        {
            count++;
            if (count >= fadeElements.Count && OnEndFadeIn != null)
                OnEndFadeIn();
        }

        private void EndFadeOut()
        {
            count++;
            if (count >= fadeElements.Count && OnEndFadeOut != null)
                OnEndFadeOut();
        }

        public void RegisterEndFadeIn(VoidDelegate _callback)
        {
            OnEndFadeIn += _callback;
        }

        public void UnregisterEndFadeIn(VoidDelegate _callback)
        {
            OnEndFadeIn -= _callback;
        }

        public void RegisterEndFadeOut(VoidDelegate _callback)
        {
            OnEndFadeOut += _callback;
        }

        public void UnregisterEndFadeOut(VoidDelegate _callback)
        {
            OnEndFadeOut -= _callback;
        }        
    }
}