﻿namespace Novelab.Fade
{
    using UnityEngine;

    [RequireComponent(typeof(CanvasGroup))]
    public class FadeInOutUICanvasGroup : FadeInOut
    {
        private CanvasGroup uiCanvasGroup;

        protected void Awake()
        {
            uiCanvasGroup = GetComponent<CanvasGroup>();
        }

        public override float GetAlpha()
        {
            return uiCanvasGroup.alpha;
        }

        public override void SetAlpha(float _alpha)
        {
            uiCanvasGroup.alpha = _alpha;
        }

    }
}