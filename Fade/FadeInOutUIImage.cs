﻿namespace Novelab.Fade
{
    using UnityEngine;
    using UnityEngine.UI;

    [RequireComponent(typeof(Image))]
    public class FadeInOutUIImage : FadeInOut
    {
        private Image uiImage;

        protected void Awake()
        {
            uiImage = GetComponent<Image>();
        }

        public override float GetAlpha()
        {
            return GetColor().a;
        }

        public override void SetAlpha(float _alpha)
        {
            Color tempColor = GetColor();
            tempColor.a = _alpha;
            uiImage.color = tempColor;
        }

        private Color GetColor()
        {
            return uiImage.color;
        }

    }
}