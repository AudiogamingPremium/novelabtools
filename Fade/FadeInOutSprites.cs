﻿namespace Novelab.Fade
{
    using System;
    using System.Collections;
    using UnityEngine;

    public class FadeInOutSprites : FadeInOut
    {
        [SerializeField]
        private SpriteRenderer[] spriteRenderers;
        public SpriteRenderer[] SpriteRenderers
        {
            get { return spriteRenderers; }
        }

        public override float GetAlpha()
        {
            if (spriteRenderers.Length == 0)
            {
                Debug.Log("[FadeInOutSprites] No sprites set to " + name);
                return 0;
            }

            return spriteRenderers[0].color.a;
        }

        public override void SetAlpha(float _alpha)
        {
            Color tempColor;
            for (int i = 0; i < spriteRenderers.Length; i++)
            {
                tempColor = spriteRenderers[i].color;
                tempColor.a = _alpha;
                spriteRenderers[i].color = tempColor;
            }
        }

    }
}