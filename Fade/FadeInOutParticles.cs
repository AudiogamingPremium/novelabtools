﻿namespace Novelab.Fade
{
    using System.Collections;
    using UnityEngine;

    public class FadeInOutParticles : FadeInOut
    {
        private ParticleSystemRenderer myPartSysRenderer;

        private void Awake()
        {
            myPartSysRenderer = GetComponent<ParticleSystemRenderer>();
        }


        public override float GetAlpha()
        {
            return myPartSysRenderer.material.GetColor("_TintColor").a;
        }

        public override void SetAlpha(float _alpha)
        {
            Color temp = myPartSysRenderer.material.GetColor("_TintColor");
            temp.a = _alpha;
            myPartSysRenderer.material.SetColor("_TintColor", temp);
        }

    }
}