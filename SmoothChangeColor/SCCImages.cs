﻿namespace Novelab.SCC
{
    using UnityEngine;
    using UnityEngine.UI;

    public class SCCImages : SmoothChangeColor
    {
        [SerializeField]
        private Image[] uiImages;

        protected override Color GetColor()
        {
            if (uiImages.Length == 0)
            {
                Debug.Log("[SCCImage] No image renderer set to " + name);
                return Color.red;
            }

            return uiImages[0].color;
        }

        protected override void SetColor(Color _color)
        {
            if (uiImages.Length == 0)
            {
                Debug.Log("[SCCImage] No mesh renderer set to " + name);
                return;
            }

            Color tempColor;

            for (int i = 0; i < uiImages.Length; i++)
            {
                if (uiImages[i] == null)
                    continue;

                tempColor = uiImages[i].color;
                _color.a = tempColor.a;
                uiImages[i].color = _color;
            }
        }
    }
}
