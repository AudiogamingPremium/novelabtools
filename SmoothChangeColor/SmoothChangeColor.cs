﻿namespace Novelab.SCC
{
    using UnityEngine;
    using Novelab.Utils;

    public abstract class SmoothChangeColor : MonoBehaviour
    {
        [SerializeField]
        protected float duration = 2;
        [SerializeField]
        protected AnimationCurve colorCurve;

        private Color startColor;
        private Color destColor;
        
        public UnityEngine.Events.UnityEvent OnEndChangeColor;

        protected Coroutine changeColorCo;

        protected abstract void SetColor(Color _color);
        protected abstract Color GetColor();

        public void ChangeColor(Color _destColor)
        {
            startColor = GetColor();
            destColor = _destColor;

            if (changeColorCo != null)
                CoroutinesManager.Instance.StopOwnCoroutine(changeColorCo);

            if (gameObject.activeInHierarchy)
                changeColorCo = CoroutinesManager.Instance.StartCurveCoroutine(UpdateColor, 0, 1, duration, colorCurve);
        }

        public void UpdateColor(float _lerp)
        {
            SetColor(Color.Lerp(startColor, destColor, _lerp));

            if (_lerp == 1)
                TrigEndChangeColor();
        }

        private void TrigEndChangeColor()
        {
            OnEndChangeColor.Invoke();
        }

        public void RegisterEndChangeColor(UnityEngine.Events.UnityAction _action)
        {
            OnEndChangeColor.AddListener(_action);
        }

        public void UnregisterEndChangeColor(UnityEngine.Events.UnityAction _action)
        {
            OnEndChangeColor.RemoveListener(_action);
        }

    }
}