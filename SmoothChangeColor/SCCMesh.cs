﻿namespace Novelab.SCC
{
    using UnityEngine;

    public class SCCMesh : SmoothChangeColor
    {
        [SerializeField]
        private MeshRenderer[] meshRenderers;
        [SerializeField]
        private string colorPropertyName = "_Color";

        protected override Color GetColor()
        {
            if (meshRenderers.Length == 0)
            {
                Debug.Log("[SCCMesh] No mesh renderer set to " + name);
                return Color.red;
            }

            return meshRenderers[0].material.GetColor(colorPropertyName);
        }

        protected override void SetColor(Color _color)
        {
            if (meshRenderers.Length == 0)
            {
                Debug.Log("[SCCMesh] No mesh renderer set to " + name);
                return;
            }

            Color tempColor;

            for (int i = 0; i < meshRenderers.Length; i++)
            {
                if (meshRenderers[i] == null)
                    continue;

                tempColor = meshRenderers[i].material.GetColor(colorPropertyName);
                _color.a = tempColor.a;
                meshRenderers[i].material.SetColor(colorPropertyName, _color);
            }
        }
    }
}