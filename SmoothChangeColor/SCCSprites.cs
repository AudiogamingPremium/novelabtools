﻿namespace Novelab.SCC
{
    using UnityEngine;

    public class SCCSprites : SmoothChangeColor
    {
        [SerializeField]
        private SpriteRenderer[] spriteRenderers;

        protected override Color GetColor()
        {
            if (spriteRenderers.Length == 0)
            {
                Debug.Log("[SCCSprites] No sprite renderer set to " + name);
                return Color.red;
            }

            return spriteRenderers[0].color;
        }

        protected override void SetColor(Color _color)
        {
            ForceColor(_color);
        }

        public void ForceColor(Color _color)
        {
            if (spriteRenderers.Length == 0)
            {
                Debug.Log("[SCCSprites] No sprite renderer set to " + name);
                return;
            }

            for (int i = 0; i < spriteRenderers.Length; i++)
            {
                if (spriteRenderers[i] == null)
                    continue;

                _color.a = spriteRenderers[i].color.a;
                spriteRenderers[i].color = _color;
            }
        }

    }
}